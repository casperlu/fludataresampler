
//Forward declared dependencies

//Included dependencies
#include "anaParam.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include "misc.h"


using namespace std;

//Constructors
AnaParam::AnaParam() : c(200), maxNt(10000), seed(1), useGreedyMMS(false), useSharedBottleneck(false), withinHostSelectionPresent(false),  withinHostSelectionFolder(""), mlWithinHostSelectionFolder(""), BICpenalty(10), selectionCapPresent(false), useIntegralApproach(false), maxNumFittedParams(10), terminateEarly(0), filterHaplotypesMethod(0), noVar(false), meanOnly(false), growthFactor(22), numGenerations(1), analyseSL(false), analysisMethod("simple"), useTrueHaps(false) {
    //Do nothing
}



AnaParam::~AnaParam() { }; //Deconstructor


//Getters
int AnaParam::getMaxNt() { return maxNt; }
unsigned long int AnaParam::getSeed() { return seed; }
void AnaParam::setSeed(int s) { seed = s; }
vector<Sequence>* AnaParam::getSelVec() { return &selVec; }
vector<vector<double> >* AnaParam::getSelMagVec() { return &selMagVec; }
double AnaParam::getC() { return c; }
void AnaParam::setC(double C) { c = C; }
vector<double>* AnaParam::getLogFactStore() { return logFactStore; }
int AnaParam::getNumPrePoints() { return numPrePoints; }


void AnaParam::loadOutputFolder(string & fullPathToFolder) { pathToFolder=fullPathToFolder; }
void AnaParam::loadUseGreedyMMS(bool value) { useGreedyMMS = value; }

string AnaParam::getOutputFolder() {
    if(pathToFolder.length() > 0) { return pathToFolder; }
    else {
        cout << "ERROR: Path to output folder not loaded. Expect segmentation fault";
        return pathToFolder;
    }
}



bool AnaParam::getUseGreedyMMS() { return useGreedyMMS; }

void AnaParam::setUseSharedBottleneck(bool value) { useSharedBottleneck = value; }
bool AnaParam::getUseSharedBottleneck() { return useSharedBottleneck; }


void AnaParam::loadWithinHostSelectionFolder(string& fullPathToFolder) { withinHostSelectionFolder = fullPathToFolder; }
void AnaParam::loadMLWithinHostSelectionFolder(string& fullPathToFolder) { mlWithinHostSelectionFolder = fullPathToFolder; }
string AnaParam::getWithinHostSelectionFolder() { return withinHostSelectionFolder; }
string AnaParam::getMLWithinHostSelectionFolder() { return mlWithinHostSelectionFolder; }


bool AnaParam::getWithinHostSelectionPresent() {
	
	for(unsigned int i=0; i<withinHostSelectionPresent.size(); i++) {

		if(withinHostSelectionPresent[i] == true) { return true; }
	}

	return false;
}

bool AnaParam::getWithinHostSelectionPresent(int gene) { 

	if(withinHostSelectionPresent.size() > 0) { //Within host selection defined
		return withinHostSelectionPresent[gene]; 
	} else {
		return false; //Within host selection not defined, so return default of no selG for this gene
	}
}

void AnaParam::addWithinHostSelectionPresent(bool b) { withinHostSelectionPresent.push_back(b); }

vector<Sequence> AnaParam::getSelGvec() { return selGvec; }
void AnaParam::setSelGvec(vector<Sequence>& SGV) { selGvec = SGV; }
vector<vector<double> > AnaParam::getSelGmagVec() { return selGmagVec; }
void AnaParam::setSelGmagVec(vector<vector<double> >& SGMV) { selGmagVec = SGMV; }
vector<vector<Model::epistasis> > AnaParam::getEpiGvec() { return epiGvec; }
void AnaParam::setEpiGvec(std::vector<std::vector<Model::epistasis> >& EGV) { epiGvec = EGV; }
vector<vector<double> > AnaParam::getEpiGmagVec() { return epiGmagVec; }
void AnaParam::setEpiGmagVec(std::vector<std::vector<double> >& EGMV) { epiGmagVec = EGMV; }



void AnaParam::setHapFitG(vector<vector<double> > & HFG) { hapFitG = HFG; }
void AnaParam::setHapFitG(vector<vector<vector<double> > > & HFG) { hapFitGmultiRep = HFG; }
vector<double> AnaParam::getHapFitG(int gene) { return hapFitG[gene]; }
vector<double> AnaParam::getHapFitG(int rep, int gene) { return hapFitGmultiRep[rep][gene]; }

void AnaParam::setBICpenalty(double p) { BICpenalty = p; }
double AnaParam::getBICpenalty() { return BICpenalty; }
void AnaParam::setSelectionCap(double value) { selectionCap = value; selectionCapPresent = true; }
double AnaParam::getSelectionCap() { return selectionCap; }
bool AnaParam::getSelectionCapPresent() { return selectionCapPresent; }
bool AnaParam::getUseIntegralApproach() { return useIntegralApproach; }
void AnaParam::setUseIntegralApproach(bool b) { useIntegralApproach = b; }
void AnaParam::setMaxNumFittedParams(int num) { maxNumFittedParams = num; }
int AnaParam::getMaxNumFittedParams() { return maxNumFittedParams; }
void AnaParam::setMaxNt(int mNt) { maxNt = mNt; }
void AnaParam::setTerminateEarly(int t) { terminateEarly = t; } //E.g. 1==After hap inference, 2==After freq inference, 3==...
int AnaParam::getTerminateEarly() { return terminateEarly; }
void AnaParam::setFilterHaplotypesMethod(int fhm) { filterHaplotypesMethod = fhm; }
int AnaParam::getFilterHaplotypesMethod() { return filterHaplotypesMethod; }
void AnaParam::setNoVar(bool b) { noVar = b; }
bool AnaParam::getNoVar() { return noVar; }
void AnaParam::setMeanOnly(bool b) { meanOnly = b; }
bool AnaParam::getMeanOnly() { return meanOnly; }
void AnaParam::setGrowthFactor(int gf) { growthFactor = gf; }
int AnaParam::getGrowthFactor() { return growthFactor; }
void AnaParam::setAnalyseSL(bool b) { analyseSL = b; }
bool AnaParam::getAnalyseSL() { return analyseSL; }
void AnaParam::setAnalysisMethod(string s) { analysisMethod = s; }
string AnaParam::getAnalysisMethod() { return analysisMethod; }
void AnaParam::setNumGenerations(int ng) { numGenerations = ng; }
int AnaParam::getNumGenerations() { return numGenerations; }
void AnaParam::setUseTrueHaps(bool b) { useTrueHaps = b; }
bool AnaParam::getUseTrueHaps() { return useTrueHaps; }
