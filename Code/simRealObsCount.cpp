#include "dataPHMG.hpp"
#include "dataPHMR.hpp"
#include "simParam.h"
#include "anaParam.h"
#include "sequence.h"
#include "misc.h"
#include "dataPH.hpp"
#include "simParamPH.h"
#include "inputParser.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <sys/stat.h>
#include <string.h>
#include <gsl/gsl_rng.h>

#ifdef _OPENMP
        #include <omp.h>
#else
        #define omp_get_thread_num() 0
#endif

using namespace std;

int main (int argc, char* argv[]) {

	//Get start time
	int timeStart = omp_get_wtime();
	
	//Create param objects
	SimParamPH spPHMG;
	AnaParam ap;

	//Initiate the input parser from the command line inputs
	InputParser input(argc, argv);

	cout << "\n\n\n ***************************** Input parameters ******************************* \n\n\n";

	//Get bottleneck size
	int Nt = -1;
	const string NtString = input.getCmdOption("-Nt");
	if (!NtString.empty()){
		cout << "Bottleneck input was: " << NtString << "\n";
		Nt = atoi(NtString.c_str());
	} else {
		Nt = 100; //Default
	}

	//Get simulation seed
	int simSeed = -1;
	const string simSeedString = input.getCmdOption("-ss");
	if (!simSeedString.empty()){
		cout << "Simulation seed input was: " << simSeedString << "\n";
		simSeed = atoi(simSeedString.c_str());
	} else {
		simSeed = 1; //Default
	}


/*
 * Do we need this?
 */
	//Get number of genes
	int numGenes = -1;
	const string numGenesString = input.getCmdOption("-ng");
	if (!numGenesString.empty()){
		cout << "Number of genes input was: " << numGenesString << "\n";
		numGenes = atoi(numGenesString.c_str());
	} else {
		numGenes = 8; //Default
		cout << "numGenes set to default: " << numGenes << "\n";
	}

	//Get geneLength (shared by all genes)
	int geneLength = -1; //Undefined
	const string geneLengthString = input.getCmdOption("-gl");
	if (!geneLengthString.empty()){
		cout << "Gene length: " << geneLengthString << "\n";
		geneLength = atoi(geneLengthString.c_str());
	}
	
	//Get number of full haps
	int numHaps = -1;
	const string numHapsString = input.getCmdOption("-nh");
	if (!numHapsString.empty()){
		cout << "Number of full haplotypes input was: " << numHapsString << "\n";
		numHaps = atoi(numHapsString.c_str()); //Should be < 2^numLoci, e.g. 2^4=16 possible full haplotypes to choose from for four loci
	} else {
		numHaps = 8; //Default
		cout << "numHaps set to default: " << numHaps << "\n";
	}

	//Get C value
	double c = -1;
	const string cString = input.getCmdOption("-C");
	if (!cString.empty()){
		cout << "C input was: " << cString << "\n";
		c = atof(cString.c_str());
	} else {
		c = 200; //Default
		cout << "C set to default: " << c << "\n";
	}

	//Filtering of simulated data
	const string filterDataSimString = input.getCmdOption("-filterDataSim");
	string filterDataSim = "Samfire"; //Default
	if(!filterDataSimString.empty()) {
		cout << "filterDataSimString input was: " << filterDataSimString << "\n";
		if(filterDataSimString.compare("Transmission")==0 || filterDataSimString.compare("transmission")==0) {
			filterDataSim = "Transmission";
		} else if(filterDataSimString.compare("Samfire")==0 || filterDataSimString.compare("samfire")==0 || filterDataSimString.compare("SAMFIRE")==0) {
			filterDataSim = "Samfire";
		} else if(filterDataSimString.compare("None")==0 || filterDataSimString.compare("none")==0) {
			filterDataSim = "None";
		} else {

			cout << "Error with filterDataSim input: Input has to be 'Transmission' or 'Samfire'.\n"; exit(1);
		}	
	}


	//Get dataset folder in full form, e.g.
	// /home/ckl35/rds/rds-cjri2-illingworthrds/ckl35/Datasets/Moncla/FolderA/FolderB/FolderC
	//Disregard final "/"!
	string inputFolder;
	stringstream ssInput;
	const string dataset = input.getCmdOption("-i");
	if(!dataset.empty()) {

		//Add dataset to input
		ssInput << dataset << "/";
		inputFolder = ssInput.str();

	} else {

		cout << "Error in getting the correct filepath!\n";
		return -1;
	}

	//Flag to use imported haplotypes and frequencies from multi_locus_trajectories.out format.
	//Reads in one set of full haplotypes and two sets of frequencies
	//(before and after transmission). String to folder containing
	//haplotypes and frequencies must be supplied. The folder must contain
	//8 subfolders corresponding to the flu genes ("HA", "MP",...)". 
	//Each subfolder must contain a file named "Inferred_haps.out" which is of the form:
	//	1	h1	qB1	qA1
	//	2	h2	qB2	qA2
	//	..
	//	k	hk	qBk	qAk
	//
	//where each column is tab delimited.
	const string importHapsString = input.getCmdOption("-importHaps");
	bool importHaps = false;
	string importHapsPath = "";
	if(!importHapsString.empty()) {

		importHaps = true;
		stringstream ss;
		ss << importHapsString << "/"; //Add '/' in case folder doesn't end in '/' itself
		importHapsPath = ss.str();
		cout << "ImportHaps input was: " << importHapsString << "\n";
	} else {

		importHaps = false; //Default, don't use imported haplotypes
	}

	//Flag to use a different filename than "Inferred_haps.out" for haplotype information
	const string importHapsFileString = input.getCmdOption("-importHapsFile");
	string importHapsFilename = "";
	if(!importHapsFileString.empty()) {

		importHapsFilename = importHapsFileString;

	} else {
		importHapsFilename = "Inferred_haps.out"; //Standard filename
	}





	//Create output folder based on full path
	//e.g. /home/ckl35/rds/rds-cjri2-illingworthrds/ckl35/Output/Moncla/Analysis/FolderA/FolderB/FolderC
	stringstream ssOutput;
	string outputFolder;
	const string outputFolderString = input.getCmdOption("-o");
	if(!outputFolderString.empty()) {

		
		//Create relevant output folders, i.e. split tempFolders string by "/"
		stringstream ssTempFolder;
		if(importHaps == true) { //No transmission in this case, so no Nt
			ssTempFolder << outputFolderString << "/Seed_" << simSeed << "/";
		} else { //Default
			ssTempFolder << outputFolderString << "/Nt_" << Nt << "/Seed_" << simSeed << "/";
		}

		string folder;
		while(getline(ssTempFolder, folder, '/')) {

			ssOutput << folder << "/";
			string outputFolderCurrent = ssOutput.str();
			//If output folder doesn't exist, create it
			if(fileExists(outputFolderCurrent)!= true) {
				cout << "Ouput folder: " << outputFolderCurrent << " created.\n";
				mkdir(outputFolderCurrent.c_str(),0775);		
			}
		}
		outputFolder = ssOutput.str();

	}
	cout << "Outputfolder is : " << outputFolder << "\n";


	const string reps = input.getCmdOption("-reps");
	bool repsPresent=false; //As standard, data is single replicate data
	if(!reps.empty()) {

		if(reps.compare("True")==0 || reps.compare("true")==0) {

			repsPresent = true;
		}	
	}

	//Filter data (read in data, not simulated data) (not haplotypes) - this is a data flag, not an analysis flag
	const string filterDataString = input.getCmdOption("-filterData");
	bool filterData = false; //Default
	if(!filterDataString.empty()) {
		cout << "filterDataString input was: " << filterDataString << "\n";
		if(filterDataString.compare("true")==0 || filterDataString.compare("True")==0) {
			filterData = true;
		} else if (filterDataString.compare("false")==0 || filterDataString.compare("False")==0) {
			filterData = false;
		} else {

			cout << "Error with filterData input: Input has to be true or false.\n"; exit(1);
		}	
	}


	//Filter haps within the C++ code
	const string filterString = input.getCmdOption("-filter");
	int filterHapsMethod=0; //Default is 0, i.e. no filtering
	if(!filterString.empty()) {

		filterHapsMethod = atoi(filterString.c_str());
	}

	//Get export format, e.g. "Multi" or "Hap"
	string format = "Multi"; //Default
	const string formatString = input.getCmdOption("-format");
	if(!formatString.empty()) {
		cout << "Format input was: " << formatString << "\n";
		format = formatString; 
	}

	//Get remove monomorphic sites in simulated data
	bool removeMonomorphicSim = false; //Default
	const string removeMonomorphicSimString = input.getCmdOption("-rmMonoSim");
	if(!removeMonomorphicSimString.empty()) {
		cout << "removeMonomorphicSim input was: " << removeMonomorphicSimString << "\n";
		if(removeMonomorphicSimString.compare("true")==0 || removeMonomorphicSimString.compare("True")==0) {
			removeMonomorphicSim = true;
		} else if (removeMonomorphicSimString.compare("false")==0 || removeMonomorphicSimString.compare("False")==0) {
			removeMonomorphicSim = false;
		} else {

			cout << "Error with removeMonomorphicSim input: Input has to be true or false.\n"; exit(1);
		}	
	}

	//Get minimum required read depth. Setting to 0 results in unfiltered outcome.
	bool usePhysicalPositions = false;
	int minReadDepth = -1;
	const string minReadDepthString = input.getCmdOption("-minRD");
	if (!minReadDepthString.empty()) {
		cout << "min read depth input was: " << minReadDepthString << "\n";
		minReadDepth = atoi(minReadDepthString.c_str());
		//TODO: WE HAVEN'T ACTUALLY IMPLEMENTED THIS!
	} else {
		minReadDepth = 100; //Default
	}

	//Follow-up: If min read depth is non zero, we need to use physical positions
	if(minReadDepth > 0) {
		usePhysicalPositions = true;
	}

	//Create file to write input parameters to
	stringstream ssInputParams;
	ssInputParams << outputFolder << "InputParams.out";
	string filePathInputParams = ssInputParams.str();
	ofstream outputFileInputParams;
	outputFileInputParams.open(filePathInputParams.c_str());
	outputFileInputParams << "Parameters used for simulation/analysis:\n";
	outputFileInputParams << "Nt: " << Nt << "\n";
	outputFileInputParams << "SimSeed: " << simSeed << "\n";
	outputFileInputParams << "NumGenes: " << numGenes << "\n";
	if(geneLength != -1) { outputFileInputParams << "GeneLength: " << geneLength << "\n"; }
	outputFileInputParams << "NumHaps: " << numHaps << "\n";
	outputFileInputParams << "C: " << c << "\n";
	outputFileInputParams << "filterDataSim: " << filterDataSim << "\n";
	outputFileInputParams << "OutputFolderString: " << outputFolderString << "\n";
	outputFileInputParams << "Reps: " << reps << "\n";
	if(importHaps == true) { outputFileInputParams << "ImportHapsString: " << importHapsString << "\n"; }
	if(filterHapsMethod > 0) { outputFileInputParams << "Filter haplotypes method: " << filterHapsMethod << "\n"; }
	outputFileInputParams << "Format: " << format << "\n";
	outputFileInputParams << "removeMonomorphicSim: " << removeMonomorphicSim << "\n";
	outputFileInputParams.close();
	
	//Generate a sel model for the data
	vector<Sequence> selVec;
	vector<vector<double> > selMagVec;


	//Get selection information
	int selGene;
	const string selGeneString = input.getCmdOption("-selGene");
	if (!selGeneString.empty()){
		cout << "SelGene input was: " << selGeneString << "\n";
		selGene = atoi(selGeneString.c_str());
	} else {
		selGene = -1; //Neutral
	}

	int selLocus;
	const string selLocusString = input.getCmdOption("-selLocus");
	if (!selLocusString.empty()){
		cout << "SelLocus input was: " << selLocusString << "\n";
		selLocus = atoi(selLocusString.c_str());
	} else {
		selLocus = -1; //Neutral
	}

	double selStrength;
	const string selStrengthString = input.getCmdOption("-selStrength");
	if (!selStrengthString.empty()){
		cout << "selStrength input was: " << selStrengthString << "\n";
		selStrength = atof(selStrengthString.c_str());
	} else {
		selStrength = 0; //Neutral
	}

	//Read in external data to get observation counts
	cout << "\n\n\n ******************* Reading in data to obtain read depths  ******************************* \n\n\n";
	DataPHMG dPHMG;
	DataPHMR dPHMR;
	if(repsPresent == false) {
		
		cout << "\nLoading filepaths for gene segments:\n";
		PathPHMG pPHMG;
		if(importHaps == true) { //Perhaps do else if
			//TODO: Update to not include "Mahan" in method name
			pPHMG.setImportHapsMahan(importHapsPath);
			pPHMG.setImportHapsMahanFilename(importHapsFilename);
		}
		if(usePhysicalPositions == true) {
			pPHMG.setPhysicalPosMatterToTrue();
		}
		pPHMG.setFilterData(filterData);
		pPHMG.loadFluFilePaths(inputFolder);
		dPHMG.readDataML(&pPHMG);

	} else {

		//Get infoFile from inputfolder
		stringstream ss;
		ss << inputFolder << "RepInfo.dat";
		string infoFilePath = ss.str();

		//Get replicate sub folders
		vector<string> repFolders;
		ifstream infoFile;
		infoFile.open(infoFilePath.c_str());
		string line;
		while(getline(infoFile, line)) {

			ss.str("");
			ss << inputFolder << line << "/";
			string repFolder = ss.str();
			repFolders.push_back(repFolder);
		}


		cout << "\nLoading filepaths for gene segments:\n";
		PathPHMR pPHMR;
		pPHMR.loadFluReplicateFilePaths(repFolders);
		dPHMR.readData(&pPHMR);

	}


	if(repsPresent == false) {
	
		//Loop over genes
		for(int i=0; i<dPHMG.getNumGenes(); i++) {

			//cout << "Gene " << i << " status: " << dPHMG.getGenePresent(i) << "\n";
			if(dPHMG.getGenePresent(i) == true) {


				//Find the haplotype length for gene i
				DataPH* dPH = dPHMG.getGene(i);
				vector<Sequence> allSeqs = dPH->getSequences();
				int hapLength = allSeqs[0].getLength();
				//cout << "Hap length: " << hapLength << "\n";

				Sequence selSeq = Sequence(hapLength); //-,-,-,-,-,-
				vector<double> selMag (hapLength, 0.0); //0,0,0,0,0,0
			
				if(i==selGene) { //Gene under selection
			
				
					//Find allele for locus under selection
					char allele = '%';
					for(unsigned int j=0; j<allSeqs.size(); j++) {
			
						if(allSeqs[j].getBase(selLocus) != '-') {

							allele = allSeqs[j].getBase(selLocus); //WLOG choose the first allele found
							cout << "Selection chosen to act on gene " << i << " on " << allele << " at locus " << selLocus << " with strength " << selStrength << ".\n";
							break;
						}
					}
	
					selSeq.setBase(selLocus, allele); //Set selection act on A's at the mid point of gene 0
					selMag[selLocus] = selStrength;
				}

				selVec.push_back(selSeq);
				selMagVec.push_back(selMag);

			} else {
		
				Sequence emptySeq;
				vector<double> emptyVec;
				selVec.push_back(emptySeq);
				selMagVec.push_back(emptyVec);
			}
		}

	} else {	
	
		vector<vector<int> > allPos = dPHMR.findAllPos();
		vector<vector<vector<int> > > mapFromAllPosToReps = dPHMR.findMapFromAllPosToReps(allPos);
		vector<vector<bool> > mapFromAllPosToShared = dPHMR.findMapFromAllPosToShared(allPos, mapFromAllPosToReps);

		
		//Loop over genes
		for(unsigned int i=0; i<allPos.size(); i++) {

			if(allPos[i].size() > 0) { //Gene has positions

				Sequence selSeq = Sequence(allPos[i].size()); //-,-,-,-,-,-
				vector<double> selMag (allPos[i].size(), 0.0); //0,0,0,0,0,0
			
				if((int) i==selGene) { //Gene under selection
			
					//Find shared selLocus
					int sharedSelLocus = -1;
					int sharedPosCounter = 0;
					for(unsigned int j=0; j<allPos[i].size(); j++) {

						if(mapFromAllPosToShared[i][j] == true) { //Pos is shared between all replicates
							sharedPosCounter++;
						}
						if(sharedPosCounter == selLocus+1) {

							sharedSelLocus = j;
							break;
						}
					}

					//Consider replicate 0 WLOG
					DataPHMG* dPHMG = dPHMR.getReplicate(0);
					DataPH* dPH = dPHMG->getGene(i);
					vector<Sequence> allSeqs = dPH->getSequences();
					
			
					//Find allele for locus under selection
					char allele = '%';
					for(unsigned int j=0; j<allSeqs.size(); j++) {
			
						if(allSeqs[j].getBase(mapFromAllPosToReps[i][0][sharedSelLocus]) != '-') {

							allele = allSeqs[j].getBase(mapFromAllPosToReps[i][0][sharedSelLocus]); //WLOG choose the first allele found
							cout << "Selection chosen to act on gene " << i << " on " << allele << " at locus " << sharedSelLocus << " with strength " << selStrength << ".\n";
							break;
						}
					}

					selSeq.setBase(sharedSelLocus, allele); 
					selMag[sharedSelLocus] = selStrength;
				}

				selVec.push_back(selSeq);
				selMagVec.push_back(selMag);

			} else {
		
				Sequence emptySeq;
				vector<double> emptyVec;
				selVec.push_back(emptySeq);
				selMagVec.push_back(emptyVec);
			}
		}	
	}

	cout << "\n\n------------------------------------------\n\n";


	
	//Set up simParam and simulate data using read statistic from external data set
	spPHMG.setC(c);
	spPHMG.setNt(Nt);
	spPHMG.setNumGenes(numGenes);
	if(geneLength != -1) { spPHMG.setGeneLength(geneLength); }
	spPHMG.setSeed(simSeed);
	spPHMG.setSelMulti(selVec);
	spPHMG.setSelMagVecMulti(selMagVec);
	spPHMG.setDim(numHaps);
	spPHMG.setPathToFolder(outputFolder);
	spPHMG.setUseExternalReadStatistic(true);
	spPHMG.setFilterHaplotypesMethod(filterHapsMethod);
	spPHMG.setFormat(format); //Define output format, e.g. "Multi"
	spPHMG.setRemoveMonomorphicSim(removeMonomorphicSim);
	spPHMG.setFilterData(filterDataSim);
	

	if(repsPresent == false) {
		cout << "Single replicate mode requested.\n";
		cout << "\n\n\n ***************************** Start of simulation ******************************* \n\n\n";
		dPHMG.simulateData(&spPHMG);
	} else {
		cout << "Multi replicate mode requested.\n";
		cout << "\n\n\n ***************************** Start of simulation ******************************* \n\n\n";
		dPHMR.simulateRealData(&spPHMG);
	} 
	

	
	//Print elapsed time to file 
	int timeFinish = omp_get_wtime();
	cout << "Elapsed time: " << timeFinish - timeStart << "\n";
	
	stringstream ssTime;
	ssTime << ssOutput.str() << "Elapsed_Time.dat";
	string filePathTime = ssTime.str();
	ofstream outputFileTime;
	outputFileTime.open(filePathTime.c_str());
	outputFileTime << timeFinish - timeStart << "\n";
	outputFileTime.close();
  
	return 0;
}

