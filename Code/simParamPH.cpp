
//Forward declared dependencies

//Included dependencies
#include "simParamPH.h"
#include <stdio.h>
#include <iostream>

using namespace std;

//Define constructors
SimParamPH::SimParamPH() : SimParam(), dim(3), C(200), repID("-1"), format(""), filterData("Transmission"), repScenario(false), geneIndex(-1), numGenerations(1), deltaDays(1), growthFactor(22), meanReadLength(119.68), stDevReadLength(136.88), meanGapLength(61.96), stDevGapLength(104.48), readDepth(102825) {
    
}

SimParamPH::~SimParamPH() {} //Deconstructor

//Setters
void SimParamPH::setDim(int d) { dim = d; }
void SimParamPH::setC(double c) { C = c; }
void SimParamPH::setPathToFolder(string path) { pathToFolder = path; }
void SimParamPH::setRepID(string ID) { repID = ID; }
void SimParamPH::setRepScenario(bool b) { repScenario = b; }
void SimParamPH::setGeneIndex(int g) { geneIndex = g; }
void SimParamPH::setFormat(string f) { format = f; }
void SimParamPH::setFilterData(string fd) { filterData = fd; }
void SimParamPH::setGrowthFactor(int gf) { growthFactor = gf; }
void SimParamPH::setNumGenerations(int ng) { numGenerations = ng; }
void SimParamPH::setDeltaDays(int dd) { deltaDays = dd; }
void SimParamPH::setMeanReadLength(double mrl) { meanReadLength = mrl; }
void SimParamPH::setStDevReadLength(double srl) { stDevReadLength = srl; }
void SimParamPH::setMeanGapLength(double mgl) { meanGapLength = mgl; }
void SimParamPH::setStDevGapLength(double sgl) { stDevGapLength = sgl; }
void SimParamPH::setReadDepth(int rd) { readDepth = rd; }

//Getters
int SimParamPH::getDim() { return dim; }
double SimParamPH::getC() { return C; }
string SimParamPH::getPathToFolder() { return pathToFolder; }
string SimParamPH::getRepID() { return repID; }
bool SimParamPH::getRepScenario() { return repScenario; }
int SimParamPH::getGeneIndex() { return geneIndex; }
string SimParamPH::getFormat() { return format; }
string SimParamPH::getFilterData() { return filterData; }
int SimParamPH::getGrowthFactor() { return growthFactor; }
int SimParamPH::getNumGenerations() { return numGenerations; }
int SimParamPH::getDeltaDays() { return deltaDays; }
double SimParamPH::getMeanReadLength() { return meanReadLength; }
double SimParamPH::getStDevReadLength() { return stDevReadLength; }
double SimParamPH::getMeanGapLength() { return meanGapLength; }
double SimParamPH::getStDevGapLength() { return stDevGapLength; }
int SimParamPH::getReadDepth() { return readDepth; }

