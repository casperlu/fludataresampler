
//Forward declared dependencies

//Included dependencies
#include "misc.h"
#include <iostream>
#include <gsl/gsl_linalg.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_gamma.h>
#include "fstream"
#include <algorithm>
#include <limits>

using namespace std;

//Defines quantities for numerical integration
const double tol = 1e-06; //Tolerance for numerical integration
const size_t maxEval = 1e07; //MaxEval == 0 signifies unlimited # of evaluations
const unsigned int intOutputDim = 1; //Number of output dimensions must be 1 - it has to be a scaler

//Rescales a vector of doubles to have a sum of 1 and no entry less than 10^-10
void rescale(vector<double> &vec) {
    
	//Get scaling factor
	double sum = 0;
	for (unsigned int i=0; i<vec.size();i++) {
		sum += vec[i];
	}
	if(sum > 0) { //If sum is zero, no need to use scalefactor
		double scaleFactor = 1.0/sum;
    
		//rescale
		for (unsigned int i=0;i<vec.size();i++) {
			vec[i] *= scaleFactor;
		}
	}

	//Check that no values are below 10^-11 as a result of rescaling
	bool changeOccurred = true;
	while(changeOccurred == true) {
	
		changeOccurred = false;

		for (unsigned int i=0; i<vec.size(); i++) {
		
			if(vec[i] < 1e-11) {

				//cout << "Vector < 1e-11: ";
				//printDoubleVector(vec);
				//cout << "\n";

				vec[i] = 1e-10;

				//Define vec2 to be subset of vec that excludes the ith entry
				vector<double> vec2;
				for(unsigned int j=0; j<vec.size(); j++) {
					if(j!=i) {
						vec2.push_back(vec[j]);
					}
				}
				
				if(vec2.size() == 1) { //Can't rescale if of length 1, just keep as is, or bump up to min size
					if(vec2[0] < 1e-11) { vec2[0] = 1e-10; }
				} else {
					rescale(vec2);
				}
			
				//Add new values to vec, but keep ith entry intact	
				for(unsigned int j=0; j<vec2.size(); j++) {
					if(j<i) {
						vec[j]=vec2[j];
					} else {
						vec[j+1]=vec2[j];
					}
				}

				rescale(vec);
				changeOccurred = true;
				break; //Leave for loop and start again
			}
		}
	}	
}

void rescaleZeroAccepted(vector<double> &vec) {

	 //Get scaling factor
	double sum = 0;
	for (unsigned int i=0; i<vec.size();i++) {
		sum += vec[i];
	}
	double scaleFactor = 1.0/sum;
    
	//rescale
	for (unsigned int i=0;i<vec.size();i++) {
		vec[i] *= scaleFactor;
	}
}

//Rescales a vector of doubles to have a sum of 1 and no entry less than min
//Min MUST be less than 0.3 (otherwise 1.5 factor is too small)
void rescaleMin(vector<double> &vec, double min) {
    
	//Get scaling factor
	double sum = 0;
	for (unsigned int i=0; i<vec.size();i++) {
		sum += vec[i];
	}

	if(sum == 0) { //If sum is zero, method breaks down. Return (1/n,1/n,...,1/n) with n the dimension of vec

		for(unsigned int i=0; i<vec.size(); i++) {

			vec[i] = 1.0/(double)vec.size();
		}

	} else { //The majority of cases go here
		double scaleFactor = 1.0/sum;
    
		//rescale
		for (unsigned int i=0;i<vec.size();i++) {
			vec[i] *= scaleFactor;
		}
	}


	//Check that no values are below 10^-11 as a result of rescaling
	bool changeOccurred = true;
	while(changeOccurred == true) {
	
		changeOccurred = false;

		for (unsigned int i=0; i<vec.size(); i++) {
		
			if(vec[i] < min) {

				//cout << "Vector < 1e-11: ";
				//printDoubleVector(vec);
				//cout << "\n";

				vec[i] = min*1.5; //Needs to be marginally bigger than min, otherwise iterations break down

				//Define vec2 to be subset of vec that excludes the ith entry
				vector<double> vec2;
				for(unsigned int j=0; j<vec.size(); j++) {
					if(j!=i) {
						vec2.push_back(vec[j]);
					}
				}

				if(vec2.size() == 1) { //Can't rescale if of length 1, just keep as is, or bump up to min size
					if(vec2[0] < min) { vec2[0] = min*1.5; }
				} else {
					rescaleMin(vec2, min);
				}			

				//Add new values to vec, but keep ith entry intact	
				for(unsigned int j=0; j<vec2.size(); j++) {
					if(j<i) {
						vec[j]=vec2[j];
					} else {
						vec[j+1]=vec2[j];
					}
				}

				rescaleMin(vec, min);
				changeOccurred = true;
				break; //Leave for loop and start again
			}
		}
	}	
}

//Method for printing vectors of doubles
void printDoubleVector(vector<double> &vec) {
	cout.precision(25);
    if(vec.size() > 0) {
        cout << "Vector = { ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            cout << vec[i] << " , ";
        }
        cout << vec[vec.size()-1] << " }\n";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
	cout.precision(6); //Change back
}

string printDoubleVectorToString(vector<double> &vec) {
    
    string output;
    if(vec.size() > 0) {
        output += "{ ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            output += to_string(vec[i]);
		output += " , ";
        }
        output += to_string( vec[vec.size()-1]);
	output += " }";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
    return output;
}


//Method for printing vectors of ints
void printIntVector(vector<int> &vec) {
    if(vec.size() > 0) {
        cout << "Vector = { ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            cout << vec[i] << " , ";
        }
        cout << vec[vec.size()-1] << " }\n";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
}

string printIntVectorToString(vector<int> &vec) {
    
    string output;
    if(vec.size() > 0) {
        output += "{ ";
        for(unsigned int i=0;i<vec.size()-1;i++) {
            output += to_string(vec[i]);
		output += " , ";
        }
        output += to_string( vec[vec.size()-1]);
	output += " }";
    } else {
        cout << "ERROR: Vector has zero length.\n";
    }
    return output;
}


vector<int> multinomialSampling(int N, vector<double> p, const gsl_rng *r) {
    
    size_t dim = p.size();
    unsigned int n[dim];
    double* pPointer = &p[0];
    
    gsl_ran_multinomial(r,dim,N,pPointer,n);
    
    vector<int> result(n, n + sizeof n / sizeof n[0]);
    
    return(result);
    
}

int sumOfVector(vector<int> intVec) {
    
    int tot = 0;
    for(unsigned int i=0; i<intVec.size(); i++) {
        tot+= intVec[i];
    }
    return tot;
}

double sumOfVector(vector<double> doubleVec) {
    
    double tot = 0;
    for(unsigned int i=0; i<doubleVec.size(); i++) {
        tot+= doubleVec[i];
    }
    return tot;
}

vector<double> subtractVectorsDouble(vector<double> &a, vector<double> &b) {
    
    vector<double> result;
    //Assume equal length
    for(unsigned int i=0; i<a.size(); i++) {
        result.push_back(a[i]-b[i]);
    }
    return result;
}

vector<double> addVectorsDouble(vector<double> &a, vector<double> &b) {
    
    vector<double> result;
    //Assume equal length
    for(unsigned int i=0; i<a.size(); i++) {
        result.push_back(a[i]+b[i]);
    }
    return result;
}


double my_gsl_linalg_LU_det (gsl_matrix * LU, int signum)
{
    size_t i, n = LU->size1;
    
    double det = (double) signum;
    
    for (i = 0; i < n; i++)
    {
        det *= gsl_matrix_get (LU, i, i);
    }
    
    return det;
}

double determinant(gsl_matrix *A) {
    
    size_t dim = A->size1;
    gsl_permutation *p = gsl_permutation_alloc(dim);
    int signum;
    gsl_matrix * tmp_ptr = gsl_matrix_alloc(dim,dim);
    gsl_matrix_memcpy(tmp_ptr,A);
    gsl_linalg_LU_decomp(tmp_ptr,p,&signum);
    return gsl_linalg_LU_det(tmp_ptr,signum);
    
}

double logMultivariateNormalPDF(vector<double> &x, vector<double> &mu, gsl_matrix *sigma) {
    
    //Set up permutation and matrix copy
    int dim= (int) sigma->size1;
    vector<double> xMinusMu = subtractVectorsDouble(x,mu);
    gsl_permutation *p = gsl_permutation_calloc(dim);
    int signum;
    gsl_matrix * tmp_ptr = gsl_matrix_calloc(dim,dim);
    gsl_matrix_memcpy(tmp_ptr,sigma); //Copy elements of sigma into tmp_ptr
    
    //Get LU decomposition
    gsl_linalg_LU_decomp(tmp_ptr,p,&signum);
    
    //Get determinant
    double det = gsl_linalg_LU_det(tmp_ptr, signum);
//	cout <<  "Determinant: " << det << "\n";
//	if(det==0) { det=1e-10; }
    
    //Get inverse
    gsl_matrix *sigmaInv = gsl_matrix_alloc(dim,dim);
    gsl_set_error_handler_off();
    int status = gsl_linalg_LU_invert(tmp_ptr,p,sigmaInv);
    if (status) {
        
	//Clean up, then return negative infinity
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    

        cout << "Matrix not positive definite. Returning probability of neg infinity.\n";
//	cout << "Determinant should be 0 in this case. Determinant was: " << det << "\n";
        return  -numeric_limits<double>::max();
    }
    
    
    //double logPrefactor= -0.5*log(pow(2*M_PI,dim)*det);
    double logPrefactor= -0.5*dim*log(2*M_PI) -0.5*log(det);
    
    //Convert xMinusMu to a gsl_vector
    gsl_vector *xMinusMuGSL = gsl_vector_alloc(dim);
    for(unsigned int i=0;i<xMinusMu.size();i++) {
        gsl_vector_set(xMinusMuGSL,i,xMinusMu[i]);
    }
    
    //Perform matrix*vector multiplication
    gsl_vector *sigmaInvXminusMu = gsl_vector_alloc(dim);
    gsl_blas_dgemv(CblasNoTrans,1.0,sigmaInv,xMinusMuGSL,0,sigmaInvXminusMu);
    
    //Perform vector*vector multiplication
    double dotProd;
    gsl_blas_ddot(xMinusMuGSL,sigmaInvXminusMu,&dotProd);
    
	//Clean up
	gsl_vector_free(xMinusMuGSL);
	gsl_vector_free(sigmaInvXminusMu);
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    
	return logPrefactor -0.5*dotProd;
}

double logMultivariateNormalPDFPrint(vector<double> &x, vector<double> &mu, gsl_matrix *sigma) {
	
	cout << "Input mu:\n";
	printDoubleVector(mu);
	cout << "Input sigma:\n";
	printMatrixMathematica(sigma);    

    //Set up permutation and matrix copy
    int dim= (int) sigma->size1;
    vector<double> xMinusMu = subtractVectorsDouble(x,mu);
	cout << "xMinusMu: "; printDoubleVector(xMinusMu);

    gsl_permutation *p = gsl_permutation_calloc(dim);
    int signum;
    gsl_matrix * tmp_ptr = gsl_matrix_calloc(dim,dim);
    gsl_matrix_memcpy(tmp_ptr,sigma); //Copy elements of sigma into tmp_ptr
    
    //Get LU decomposition
    gsl_linalg_LU_decomp(tmp_ptr,p,&signum);
    
    //Get determinant
    double det = gsl_linalg_LU_det(tmp_ptr, signum);
	cout << "Determinant: " << det << "\n";
    
    //Get inverse
    gsl_matrix *sigmaInv = gsl_matrix_alloc(dim,dim);
    gsl_set_error_handler_off();
    int status = gsl_linalg_LU_invert(tmp_ptr,p,sigmaInv);
    if (status) {
        
	//Clean up, then return negative infinity
	gsl_matrix_free(sigmaInv);
	gsl_matrix_free(tmp_ptr);
	gsl_permutation_free(p);
    

        cout << "Matrix not positive definite. Returning probability of neg infinity.\n";
        return  -numeric_limits<double>::max();
    }
    
	cout << "Sigma inverse: "; printMatrix(sigmaInv);
    
    //double logPrefactor= -log(sqrt(pow(2*M_PI,dim)*det));
    double logPrefactor= -0.5*dim*log(2*M_PI) -0.5*log(det);
	cout << "logPrefactor: " << logPrefactor << "\n";
    
    //Convert xMinusMu to a gsl_vector
    gsl_vector *xMinusMuGSL = gsl_vector_alloc(dim);
    for(unsigned int i=0;i<xMinusMu.size();i++) {
        gsl_vector_set(xMinusMuGSL,i,xMinusMu[i]);
    }
    
    //Perform matrix*vector multiplication
    gsl_vector *sigmaInvXminusMu = gsl_vector_alloc(dim);
    gsl_blas_dgemv(CblasNoTrans,1.0,sigmaInv,xMinusMuGSL,0,sigmaInvXminusMu);
    
    //Perform vector*vector multiplication
    double dotProd;
    gsl_blas_ddot(xMinusMuGSL,sigmaInvXminusMu,&dotProd);
	cout << "dotProduct: " << dotProd << "\n";    

    //Clean up
    gsl_vector_free(xMinusMuGSL);
    gsl_vector_free(sigmaInvXminusMu);
    gsl_matrix_free(sigmaInv);
    gsl_matrix_free(tmp_ptr);
    gsl_permutation_free(p);
    
    return logPrefactor -0.5*dotProd;
}

void printMatrix(gsl_matrix *A) {
    
    int dim1= (int) A->size1;
    int dim2= (int) A->size2;
    
    for (int i = 0; i < dim1; i++)  /* OUT OF RANGE ERROR */
        for (int j = 0; j < dim2; j++)
            printf ("m(%d,%d) = %g\n", i, j, gsl_matrix_get (A, i, j));
    
    
}

//Outputs matrix to screen in mathematica format A = {{A11,A12,A13},{A21,A22,A23},{A31,A32,A33}}
void printMatrixMathematica(gsl_matrix *A) {
    
    int dim1= (int) A->size1;
    int dim2= (int) A->size2;
    

	cout << "{";
	for (int i = 0; i < dim1; i++) {
		cout << "{";
		for (int j = 0; j < dim2; j++) {
			cout.precision(25);
			cout << gsl_matrix_get(A,i,j);
			cout.precision(6); //Back to default
			if(j<dim2-1) {
				cout << ",";
			}
  		}
		cout << "}";
		if(i<dim1-1) {
			cout << ",";
		}
	}
	cout << "}\n"; 
}

string printMatrixMathematicaToString(gsl_matrix* A) {
    
	stringstream output;
	int dim1= (int) A->size1;
	int dim2= (int) A->size2;   

	output << "{";
	for (int i = 0; i < dim1; i++) {
		output << "{";
		for (int j = 0; j < dim2; j++) {
			output << gsl_matrix_get(A,i,j);
			if(j<dim2-1) {
				output << ",";
			}
   		}
		output << "}";
		if(i<dim1-1) {
			output << ",";
		}
	}
	output << "}";

	return output.str();
}

//Create a total of number distinct haplotypes of length length
vector<Sequence> generateRandomFullHaplotypes(int length, int number, gsl_rng *r) {
    
    vector<Sequence> fhSeqs;
    Sequence temp;
    for(int i=0;i<number;i++) {
        fhSeqs.push_back(temp);
        for(int j=0;j<length;j++) {
            fhSeqs[i].addBase('-');
        }
    }
    if(number > pow(4,length)) {
        cout << "Number ("<<number<<") too large. Can't create that many distinct haplotypes with a length of only "<<length<<".\n";
        return fhSeqs; //returns -1 at all bases
    }
    
    //A=0,C=1,G=2,T=3
    fhSeqs[0]=randomSequence(length,r);
    for(int i=1;i<number;i++) {
        Sequence temp;
        bool unique = false;
        while(unique==false) {
            unique=true;
            temp = randomSequence(length, r);
            for(int j=0;j<i;j++) {
                if(identicalSeqs(temp,fhSeqs[j]) == true) { unique = false; break; }
            }
        }
        fhSeqs[i]=temp;
    }
    
    return fhSeqs;
}


Sequence randomSequence(int length, gsl_rng *r) {
    
    Sequence result;
    for(int i=0;i<length;i++) {
        int temp = (int) gsl_rng_uniform_int(r,4);
        if(temp==0) {
            result.addBase('A');
        } else if (temp==1) {
            result.addBase('C');
        } else if (temp==2) {
            result.addBase('G');
        } else {
            result.addBase('T');
        }
    }
    return result;
}

vector<Sequence> generateMajorMinorFullHaplotypes(int length, int number, gsl_rng *r) {
    
    vector<Sequence> haps;
    for(int i=0;i<number;i++) { haps.push_back(Sequence(length)); }
    
    if(number > pow(2,length)) {
        cout << "Number ("<<number<<") too large. Cannot make that many distinct haplotypes of length "<<length<<".\n";
        return haps;
    }
    
    for(int i=0;i<length;i++) {
        
        //Find major allele for locus i
        int majorInt = (int) gsl_rng_uniform_int(r,4);
        char major;
        if(majorInt==0) { major = 'A'; }
        else if(majorInt==1) { major = 'C'; }
        else if(majorInt==2) { major = 'G'; }
        else { major = 'T'; }
        
        //Find minor allele
        bool uniqueAllele = false;
        char minor = '_';
        while(uniqueAllele == false) {
            int minorInt = (int) gsl_rng_uniform_int(r,4);
            if(minorInt==0) { minor = 'A'; }
            else if(minorInt==1) { minor = 'C'; }
            else if(minorInt==2) { minor = 'G'; }
            else { minor = 'T'; }
            if(minor != major) { uniqueAllele = true; }
        }
        
        double MAF = 0.3;
        for(int j=0;j<number;j++) {
            if(gsl_rng_uniform(r) > MAF) { haps[j].setBase(i,major); }
            else { haps[j].setBase(i,minor); }
        }
    }
    //Printing
    //for(int i=0;i<number;i++) { haps[i].print(); }
    //cout << length << " " << number << "\n";
    
    return haps;
    
}

bool identicalSeqs(Sequence & a, Sequence & b) {
    
    int lengthA = a.getLength();
    if(lengthA != b.getLength()) {
        cout << "ERROR in identicalSeqs: Sequences a and b are of different lengths!!\n";
        cout << "a = ";
        a.print();
        cout << "b = ";
        b.print();
        cout << "Returning false.\n";
        return false;
    } else {
        
        for(int i=0;i<lengthA;i++) {
            if(a.getBase(i) != b.getBase(i)) {
                return false;
            }
        }
        return true;
    }
    
}

bool identicalIntVectors(std::vector<int> & a, std::vector<int> & b) {

	int lengthA = (int) a.size();
	
	if(lengthA != (int) b.size()) {
		cout << "ERROR in identicalIntVectors: vectors a and b are of different lengths!!\n";
		cout << "a = ";
		printIntVector(a);
		cout << "b = ";
		printIntVector(b);
        cout << "Returning false.\n";
        return false;
    } else {
        
		for(int i=0;i<lengthA;i++) {
            if(a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
   
}

bool nonZeroIntVector(vector<int> &a) {
    
    for(unsigned int i=0;i<a.size();i++) {
        
        if(a[i] == 0) { return false; }
    }
    
    return true;
}

bool nonZeroDoubleVector(vector<double> &a) {
    
    for(unsigned int i=0;i<a.size();i++) {
        
        if(a[i] == 0) { return false; }
    }
    
    return true;
}

void setupRNG(gsl_rng *r, unsigned long int seed) {
    
    const gsl_rng_type * T; //ISSUE HERE: Cannot define T here. Will be deleted on exit
    gsl_rng_env_setup();
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    gsl_rng_set(r,seed);
    
    cout << "RNG set up.n";
}

//Generates diploid sequence where allele1 and allele2 differ from each other at all positions
DiploidSequence generateRandomDiploidSequence(int length, gsl_rng *r) {
    
    Sequence major = randomSequence(length,r);
    Sequence minor;
    
    DiploidSequence result;
    result.setMajor(major);
    
    for(int i=0;i<length;i++) {
        char current = '-';
        bool alleleDifferentFromMajor = false;
        
        while(alleleDifferentFromMajor==false) {
            int temp = (int) gsl_rng_uniform_int(r,4);
            if(temp==0) {
                current = 'A';
            } else if (temp==1) {
                current = 'C';
            } else if (temp==2) {
                current = 'G';
            } else {
                current = 'T';
            }
            
            if(current != major.getBase(i)) {
                alleleDifferentFromMajor = true;
            }
        }
        minor.addBase(current);
    }
    
    result.setMinor(minor);
    
    return result;
    
}

//Generates diploid sequence where allele1 and allele2 differ from each other at all positions
//Constraint in the form of sequence, e.g. A--T-
DiploidSequence generateRandomSemiConstrainedDiploidSequence(Sequence constraint, gsl_rng *r) {
    
	int length = constraint.getLength();
    Sequence major = randomSequence(length,r);
    Sequence minor;
    
    for(int i=0;i<length;i++) { //Loop over sequence
	
	//Check if major allele need to be updated according to constraint
	if(constraint.getBase(i) != '-') {
		major.setBase(i, constraint.getBase(i));
	}

        char current = '-';
        bool alleleDifferentFromMajor = false;
        
        while(alleleDifferentFromMajor==false) {
            int temp = (int) gsl_rng_uniform_int(r,4);
            if(temp==0) {
                current = 'A';
            } else if (temp==1) {
                current = 'C';
            } else if (temp==2) {
                current = 'G';
            } else {
                current = 'T';
            }
            
            if(current != major.getBase(i)) {
                alleleDifferentFromMajor = true;
            }
        }
        minor.addBase(current);
    }
    
 	DiploidSequence result;
	result.setMajor(major);
    	result.setMinor(minor);
    
    return result;
    
}


//Generates diploid sequence where allele1 and allele2 differ from each other at all positions
//Constraint in the form of sequence, e.g. A/C,-/-,-/-,T/G,-/-
DiploidSequence generateRandomSemiConstrainedDiploidSequence(DiploidSequence constraint, gsl_rng *r) {
    
	int length = constraint.getLength();
	Sequence major = randomSequence(length,r); //ACGTACAGAT (random)
	Sequence minor = Sequence(length); //--------- (undefined)
    
	for(int i=0;i<length;i++) { //Loop over sequence
	
		//Check if locus need to be updated according to constraint
		bool updateMinor = false;
		//Fully constrained case (when both major and minor alleles must be specified)
		if(constraint.getMajor().getBase(i) != '-' && constraint.getMinor().getBase(i) != '-') {
			major.setBase(i, constraint.getMajor().getBase(i));
			minor.setBase(i, constraint.getMinor().getBase(i));
	
		} else if(constraint.getMajor().getBase(i) != '-') { //Semi constraint case (when the minor allele doesn't matter)

			major.setBase(i, constraint.getMajor().getBase(i));
			updateMinor = true;
		
		} else { //Unconstrained case
			updateMinor = true;
		}			


		if(updateMinor == true) {//Update minor allele to be random, but different from major allele

			char current = '-';
			bool alleleDifferentFromMajor = false;
        
			while(alleleDifferentFromMajor==false) {
				int temp = (int) gsl_rng_uniform_int(r,4);
				if(temp==0) {
					current = 'A';
				} else if (temp==1) {
					current = 'C';
				} else if (temp==2) {
					current = 'G';
				} else {
					current = 'T';
				}
            
				if(current != major.getBase(i)) {
					alleleDifferentFromMajor = true;
				}
			}
			minor.setBase(i, current);
		}
	}
    
 	DiploidSequence result;
	result.setMajor(major);
    	result.setMinor(minor);
    
	return result;
    
}

vector<Sequence> generateFullHapsFromAlleles(DiploidSequence &fullHapAlleles) {
    
    //Original data
    vector<Sequence> fullHaps;
    int lengthFH = fullHapAlleles.getMajor().getLength();
    Sequence major = fullHapAlleles.getMajor();
    Sequence minor = fullHapAlleles.getMinor();

	cout << "'Major' alleles: "; major.print();
	cout << "'Minor' alleles: "; minor.print();
    
    //Restructured data
    vector<vector<char> > fh;
    for(int i=0;i<lengthFH;i++) {
        fh.push_back({major.getBase(i), minor.getBase(i)});
    }

	for(int n=0; n<lengthFH; n++) { //Loop over loci

		if(n==0) {

			//Create the 0th locus alleles
			Sequence seq1, seq2;
			seq1.addBase(fh[0][0]);
			seq2.addBase(fh[0][1]);
			fullHaps.push_back(seq1);
			fullHaps.push_back(seq2);

		} else {

			//For nth locus, duplicate current sequences and add either fh[n][0] or fh[n][1] to the end
			vector<Sequence> fullHapsDup = fullHaps;

			for(unsigned int i=0; i<fullHaps.size(); i++) {

				fullHaps[i].addBase(fh[n][0]);
				fullHapsDup[i].addBase(fh[n][1]);
			}

			//Add fullHapsDup to fullHaps
			for(unsigned int i=0; i<fullHapsDup.size(); i++) {

				fullHaps.push_back(fullHapsDup[i]);
			}
		}


		//cout << "Printing haps at step " << n << " of " << lengthFH-1 << ":\n";
//		for(unsigned int i=0; i<fullHaps.size(); i++) {
//
///			fullHaps[i].print();
//		}

	}

        return fullHaps;
}


void randomiseSequenceVector(vector<Sequence> & s, gsl_rng *r) {
    
    int length = (int) s.size();
    int numSwaps = length * 10;
    
    for(int i=0; i<numSwaps;i++) {
        
        int swap1 = gsl_rng_uniform_int(r,length);
        int swap2 = gsl_rng_uniform_int(r,length);
        
        Sequence seq1 = s[swap1];
        Sequence seq2 = s[swap2];
        
        s[swap1] = seq2;
        s[swap2] = seq1;
        
    }
    
}

//Picks length random (but not identical) ints in the range [0,max)
vector<int> pickRandomInts(gsl_rng *r, int max, int length) {
    
    vector<int> result;
    
    if(max >= length) {
        for(int i=0;i<length;i++) {
            
            int pick;
            bool unique = false;
            while(unique == false) {
                pick = gsl_rng_uniform_int(r,max);
                
                unique = true; //Assume it is true, then check if it actually is
                for(unsigned int j=0; j<result.size(); j++) {
                    if(result[j] == pick) { unique = false; break; }
                }
            }
            result.push_back(pick);
        }
    } else {
        cout << "ERROR IN pickRandomInts. max < length. Max: " << max << ", length: " << length << "\n";
    }
    
    return result;
}


//Method for adding a random small delta to a random frequency index
void addRandom(vector<double> &oldVec, vector<double> &newVec, double delta, gsl_rng *r) {
    
    //Set newFreqs to oldFreqs values
    newVec = oldVec;

	//Update index of newFreq
	int index = gsl_rng_uniform_int(r,oldVec.size());
	double newValue = newVec[index] + delta*(gsl_rng_uniform_pos(r) - 0.5);
	if(newValue < 1e-11) {
		newValue = 1e-10;
	}
	newVec[index] = newValue;

	rescale(newVec);
}

//Method for adding a random small delta to a random frequency index
//None of the entries may be less than min
void addRandomMin(vector<double> &oldVec, vector<double> &newVec, double delta, gsl_rng *r, double min) {
    
    //Set newFreqs to oldFreqs values
    newVec = oldVec;

	//Update index of newFreq
	int index = gsl_rng_uniform_int(r,oldVec.size());
	double newValue = newVec[index] + delta*(gsl_rng_uniform_pos(r) - 0.5);
	if(newValue < min) {
		newValue = 1.5*min;
	}
	newVec[index] = newValue;

	rescaleMin(newVec, min);
}

//Method for adding a random small delta to a random frequency index
void addRandom(vector<double> & vec, double delta, gsl_rng *r) {
    
	//Update index of newFreq
	int index = gsl_rng_uniform_int(r,vec.size());
	double newValue = vec[index] + delta*(gsl_rng_uniform_pos(r) - 0.5);
	if(newValue < 1e-11) {
		newValue = 1e-10;
	}
	vec[index] = newValue;

	rescale(vec);
}

double logMultinomialProb(vector<double> & freq, vector<int> & obs) {
    
    double freqArray[(int) freq.size()];
    copy(freq.begin(), freq.end(), freqArray);
    
    unsigned int obsArray[(int) obs.size()];
    copy(obs.begin(), obs.end(), obsArray);
    
    return gsl_ran_multinomial_lnpdf(freq.size(), freqArray, obsArray);
}

//Store the log of the factorials of 0 to N in fact_store
void findLogFactorial(vector<double>& fact_store,int N){
    double logN=0;
    fact_store.push_back(0);
    for (int i=1;i<=N;i++) {
        logN=logN+log(i);
        fact_store.push_back(logN);
    }
}

//Dirichlet multinomial function taking vectors of observations and inferred frequencies as inputs
double logDirMultProbC(double C, vector<int> &obs, vector<double> &inf, vector<double> &fact_store) {
    
    int N = sumOfVector(obs);
    double bin = 0;
    
    if (N>0) {
        bin=fact_store[N]; //logN! == lnGamma(N+1)
        for (unsigned int i=0;i<obs.size();i++) {
            bin=bin-fact_store[obs[i]];
        }
        vector<double> alpha;
        for (unsigned int i=0;i<inf.size();i++) {
            alpha.push_back(C*inf[i]);
        }
        double a=0;
        for (unsigned int i=0;i<alpha.size();i++) {
            a=a+alpha[i];
            bin=bin-gsl_sf_lngamma(alpha[i]);
        }
        bin=bin+gsl_sf_lngamma(a);
        a=0;
        for (unsigned int i=0;i<alpha.size();i++) {
            double b=alpha[i]+obs[i];
            a=a+b;
            bin=bin+gsl_sf_lngamma(b);
        }
        bin=bin-gsl_sf_lngamma(a);
    } else {
        bin=0;
    }
    
    return(bin);
}

//Dirichlet multinomial function taking vectors of observations and inferred frequencies as inputs
double logDirMultProb(vector<double> &alpha, vector<int> &x, int& n) {
    

	double sumAlpha = 0;
	double sum = 0;
	for(unsigned int i=0; i<alpha.size(); i++) {
		sumAlpha += alpha[i];
		sum += gsl_sf_lngamma(x[i]+alpha[i]) - gsl_sf_lngamma(x[i]+1) - gsl_sf_lngamma(alpha[i]);
	}
	double logL = gsl_sf_lngamma(n+1) + gsl_sf_lngamma(sumAlpha) - gsl_sf_lngamma(n+sumAlpha) + sum;

	return(logL);
}

double logBetaBinProb(double alpha, double beta, int x, int n) {


	return(gsl_sf_lngamma(n+1)+gsl_sf_lngamma(x+alpha)+gsl_sf_lngamma(n-x+beta)+gsl_sf_lngamma(alpha+beta)-gsl_sf_lngamma(x+1)-gsl_sf_lngamma(n-x+1)-gsl_sf_lngamma(n+alpha+beta)-gsl_sf_lngamma(alpha)-gsl_sf_lngamma(beta));
}

//Simple computation of CDF for beta binomial. Will be slow for large threshold and n.
double BetaBinCDF(double alpha, double beta, double threshold, int n) {

	if(threshold < 0) { 
		return 0;
	} else if(threshold >= n) {
		 return 1;
	} else {

		double prob = 0;
		for(int x=0; x<=threshold; x++) {

			prob += exp(logBetaBinProb(alpha,beta,x,n));
		}
		return prob;
	}
}


//Check if a file exists. Give full path to file
bool fileExists (string& fileName) {
    ifstream f(fileName.c_str());
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }
}

//Mean and var are reduced by 1 dimension (namely the last (kth) dimension)
vector<double> computeAlpha(vector<double>& mean, gsl_matrix* var, int& n) {


	//Simple alpha_0 created from the 0,0 component only
	//q_i,j = var_i,j/(mean_i * (delta_i,j - mean_j/n))
//	double q_00 = gsl_matrix_get(var,0,0)/(mean[0]*(1-mean[0]/n));
//	cout << "q_00: " << q_00 << "\n";
//	double alpha_0 = (n-q_00)/(q_00-1);
//	cout << "alpha_0: " << alpha_0 << "\n";

	//Alpha_0 created from the average of all alpha_0_i,j
	double alpha_0_sum = 0;
	int numIncluded = 0;
	for(unsigned int i=0; i<mean.size(); i++) {
		for(unsigned int j=0; j<mean.size(); j++) {

			int KroneckerDelta = 0;
			if(i==j) { KroneckerDelta= 1; }
			double q_ij = gsl_matrix_get(var,i,j)/(mean[i]*(KroneckerDelta-mean[j]/n));
			double alpha_0_ij = (n-q_ij)/(q_ij-1);
//			cout << "alpha_o_" << i << "," << j <<": " << alpha_0_ij << "\n";
			if(alpha_0_ij > 0) {
				alpha_0_sum += alpha_0_ij;
				numIncluded++;
			}
		}
	}
	cout << "alpha_0_sum: " << alpha_0_sum << "\n";
	//double alpha_0 = alpha_0_sum/(mean.size()*mean.size());
	double alpha_0 = alpha_0_sum/numIncluded;
	cout << "alpha_0: " << alpha_0 << "\n";
//	if(alpha_0<=0) { alpha_0 = 0.01; }
	if(alpha_0_sum == 0) {
	
		for(unsigned int i=0; i<mean.size(); i++) {
			for(unsigned int j=0; j<mean.size(); j++) {

				int KroneckerDelta = 0;
				if(i==j) { KroneckerDelta= 1; }
				cout << "var_" << i << "_" << j << ": " << gsl_matrix_get(var,i,j) << "\n";
				cout << "mean_" << i << ": " << mean[i] << "\n";
				cout << "mean_" << j << ": " << mean[j] << "\n";
				double q_ij = gsl_matrix_get(var,i,j)/(mean[i]*(KroneckerDelta-mean[j]/n));
				double alpha_0_ij = (n-q_ij)/(q_ij-1);
				cout << "alpha_o_" << i << "," << j <<": " << alpha_0_ij << "\n";
				if(alpha_0_ij > 0) {
					alpha_0_sum += alpha_0_ij;
					numIncluded++;
				}
			}
		}

	}

	
	//Create full dimensional alpha
	vector<double> alpha;
	double sumMean = 0;
	for(unsigned int i=0; i<mean.size(); i++) {

		alpha.push_back(alpha_0*mean[i]/n);
		sumMean += mean[i];
	}
	//Do kth dimension
	alpha.push_back(alpha_0*(n-sumMean)/n);
	cout << "alpha: "; printDoubleVector(alpha);
	cout << "mean: "; printDoubleVector(mean);
	cout << "sumMean: " << sumMean << " n: " << n << "\n";
	
	return(alpha);

}

vector<int> DirMultSampling(int N, vector<double> &freqs, double C, const gsl_rng *r) {
    
    //Define the prior, alpha, for the Dirichlet distribution
    double alpha[freqs.size()];
    for(unsigned int i=0;i<freqs.size();i++) {
        alpha[i] = C*freqs[i];
    }
    
    //Sample frequencies p from the Dirichlet distribution
    double p[freqs.size()]; //Placeholder
    gsl_ran_dirichlet(r, freqs.size(), alpha, p);
    
    //Convert p to a vector
    vector<double> pVec;
    for(unsigned int i=0;i<freqs.size();i++) {
        pVec.push_back(p[i]);
    }
    
    //Return multinomial sample based on Dirichlet prior
    return multinomialSampling(N,pVec,r);
}


//Takes an already subsetted vector x !
void constructMatrixM(vector<double> x, gsl_matrix *M) {

    //Create subset of vec
    int dim = (int) x.size();

	//Hardcode the very simple cases as it gives a considerable speed-up. See mMatrixChecker.cpp.
	if(dim==1) { //Very simple case, if x={x1} then M = {{ x1-x1^2 }}, hardcode for efficiency. About 1.5 times as fast

		double value = x[0] - x[0]*x[0];
		gsl_matrix *mat = gsl_matrix_alloc(dim,dim);
		gsl_matrix_set(mat,0,0,value);
		gsl_matrix_memcpy(M,mat); //Copy mat into M, entry by entry
		gsl_matrix_free(mat); //Clean up
		return;

	} else if(dim==2) { //Also simple case. Here M[{x1,x2}] = {{x1 - x1^2, -x1 x2}, {-x1 x2, x2 - x2^2}}. About 1.75 times as fast.

		gsl_matrix *mat = gsl_matrix_alloc(dim,dim);
		gsl_matrix_set(mat,0,0,x[0]-x[0]*x[0]);
		gsl_matrix_set(mat,1,0,-x[0]*x[1]);
		gsl_matrix_set(mat,0,1,-x[0]*x[1]);
		gsl_matrix_set(mat,1,1,x[1]-x[1]*x[1]);
		gsl_matrix_memcpy(M,mat); //Copy mat into M, entry by entry
		gsl_matrix_free(mat); //Clean up
		return;

	}
    
    //Create diagonal matrix with elements of x
    gsl_matrix *diagX = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            
            if(j==k) {
                gsl_matrix_set(diagX,j,k,x[j]);
            } else {
                gsl_matrix_set(diagX,j,k,0);
            }
        }
    }
    
    /*
     / Create outer product of x*x^transpose.
     / This method does not exist in CBLAS library, but can be obtained otherwise.
     / In particular, Outer(a,b) = Matrix(columns=a) * DiagonalMatrix(b).
     */
    //Create matrices
    gsl_matrix *A = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            gsl_matrix_set(A,j,k,x[j]);
        }
    }
    
    //Multiply them and store in XX
    gsl_matrix *XX=gsl_matrix_alloc(dim,dim);
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, A, diagX,
                    0.0, XX);
    
    //Create M=diag(x) -xxp^t
    gsl_matrix_sub(diagX,XX); //diagX=diagX-XX, i.e. store result in diagPb
    
    //Clean up
    gsl_matrix_memcpy(M,diagX); //Copy to M, entry by entry
    gsl_matrix_free(diagX);
    gsl_matrix_free(A);
    gsl_matrix_free(XX);   
}




//Takes an already subsetted vector x !
void constructMatrixMoriginal(vector<double> x, gsl_matrix *M) {

    //Create subset of vec
    int dim = (int) x.size();
   
    //Create diagonal matrix with elements of x
    gsl_matrix *diagX = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            
            if(j==k) {
                gsl_matrix_set(diagX,j,k,x[j]);
            } else {
                gsl_matrix_set(diagX,j,k,0);
            }
        }
    }
    
    /*
     / Create outer product of x*x^transpose.
     / This method does not exist in CBLAS library, but can be obtained otherwise.
     / In particular, Outer(a,b) = Matrix(columns=a) * DiagonalMatrix(b).
     */
    //Create matrices
    gsl_matrix *A = gsl_matrix_alloc(dim,dim);
    for(int j=0; j<dim; j++) {
        for(int k=0; k<dim; k++) {
            gsl_matrix_set(A,j,k,x[j]);
        }
    }
    
    //Multiply them and store in XX
    gsl_matrix *XX=gsl_matrix_alloc(dim,dim);
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
                    1.0, A, diagX,
                    0.0, XX);
    
    //Create M=diag(x) -xxp^t
    gsl_matrix_sub(diagX,XX); //diagX=diagX-XX, i.e. store result in diagPb
    
    //Clean up
    gsl_matrix_memcpy(M,diagX); //Copy to M, entry by entry
    gsl_matrix_free(diagX);
    gsl_matrix_free(A);
    gsl_matrix_free(XX);   
}


//Compute the fitness of the full haplotypes fullHaps given the selection model selModel with coefficients selCoefsNew
//SHOULD THIS TAKE INTO ACCOUNT EPISTASIS? Is this method still in use?
vector<double> computeHapFit(vector<Sequence> &fullHaps, Sequence &selModel, vector<double> &selCoefsNew) {
    
    vector<double> hapFit(fullHaps.size(),0);
    for(unsigned int l=0;l<fullHaps.size();l++) { //Loop over haplotypes
        for(int m=0;m<fullHaps[l].getLength();m++) { //For each locus in haplotype
            //Compare nucleotide to selected nucleotide
            if(selModel.getBase(m) != '-') { //Locus under selection
                if(fullHaps[l].getBase(m)==selModel.getBase(m)) { //Haplotype under selection
                    hapFit[l]=hapFit[l]+selCoefsNew[m];
                }
            }
        }
    }
    
    return hapFit;
}

vector<double> computeSimulationHapFitT(vector<Sequence> &fullHaps, SimParamPH *spPH) {

	vector<double> hapFit(fullHaps.size(), 0);
	
	Sequence selSeq = *(spPH->getSel());
	vector<double> selMagVec = *(spPH->getSelMagVec());	

	//Selection coefficents
    for(unsigned int l=0;l<fullHaps.size();l++) { //Loop over haplotypes
        for(int m=0;m<fullHaps[l].getLength();m++) { //For each locus in haplotype
            //Compare nucleotide to selected nucleotide
            if(selSeq.getBase(m) != '-') { //Locus under selection
                if(fullHaps[l].getBase(m)==selSeq.getBase(m)) { //Haplotype under selection
                    hapFit[l]=hapFit[l]+selMagVec[m];
                }
            }
        }
    }

	vector<vector<int> > epiPosVec = *(spPH->getEpiPosVec());
	vector<double> epiMagVec = *(spPH->getEpiMagVec());
	

	//Epistasis
	for(unsigned int i=0; i<fullHaps.size(); i++) { //Loop over haplotypes
	
		//cout << "EpiPosVec.size() " << epiPosVec.size() << "\n";
		for(unsigned int j=0; j<epiPosVec.size(); j++) { //Loop over epistatic effecs

			vector<int> currentPos = epiPosVec[j];
			bool epistasisPresent = true; //Assume tru, then check
			for(unsigned int k=0; k<currentPos.size(); k++) { //Loop over positions in epi model
		
				if(fullHaps[i].getBase(currentPos[k]) != selSeq.getBase(currentPos[k])) {

					epistasisPresent = false;
					break;
				}
			}

			if(epistasisPresent == true) {
				hapFit[i] += epiMagVec[j];
			}
			cout << "HapFit[i] = " << hapFit[i] << "\n";
		}
	}
    
    return hapFit;
}

vector<double> computeSimulationHapFitG(vector<Sequence> &fullHaps, SimParamPH *spPH) {

	vector<double> hapFitG(fullHaps.size(), 0);
	
	Sequence selSeqG = *(spPH->getSelG());
	vector<double> selMagVecG = *(spPH->getSelMagVecG());	

	//Selection coefficents
    for(unsigned int l=0;l<fullHaps.size();l++) { //Loop over haplotypes
        for(int m=0;m<fullHaps[l].getLength();m++) { //For each locus in haplotype
            //Compare nucleotide to selected nucleotide
            if(selSeqG.getBase(m) != '-') { //Locus under selection
                if(fullHaps[l].getBase(m)==selSeqG.getBase(m)) { //Haplotype under selection
                    hapFitG[l]=hapFitG[l]+selMagVecG[m];
                }
            }
        }
    }

	vector<vector<int> > epiPosVecG = *(spPH->getEpiPosVecG());
	vector<double> epiMagVecG = *(spPH->getEpiMagVecG());
	

	//Epistasis
	for(unsigned int i=0; i<fullHaps.size(); i++) { //Loop over haplotypes
	
		//cout << "EpiPosVecG.size() " << epiPosVecG.size() << "\n";
		for(unsigned int j=0; j<epiPosVecG.size(); j++) { //Loop over epistatic effecs

			vector<int> currentPos = epiPosVecG[j];
			bool epistasisPresent = true; //Assume tru, then check
			for(unsigned int k=0; k<currentPos.size(); k++) { //Loop over positions in epi model
		
				if(fullHaps[i].getBase(currentPos[k]) != selSeqG.getBase(currentPos[k])) {

					epistasisPresent = false;
					break;
				}
			}

			if(epistasisPresent == true) {
				hapFitG[i] += epiMagVecG[j];
			}
			cout << "HapFitG[i] = " << hapFitG[i] << "\n";
		}
	}
    
    return hapFitG;
}

vector<DiploidSequence> createOneLocusCollapsed(vector<vector<DiploidSequence> > &oneLocusModels) {

	vector<DiploidSequence> oneLocusCollapsed;
    for(unsigned int i=0; i<oneLocusModels.size(); i++) { //Loop over selection models
        
     
        for(unsigned int j=0; j<oneLocusModels[i].size();j++) { //Loop over genes within sel model
            if(i==0) { //First time around, add an empty sequence (i.e. one for each gene)
                oneLocusCollapsed.push_back(DiploidSequence(oneLocusModels[i][j].getMajor().getLength()));
            }
            
            for(int k=0; k<oneLocusModels[i][j].getMajor().getLength(); k++) { //Loop over length of sequence motif
             
                if(oneLocusModels[i][j].getMajor().getBase(k) != '-') { //If locus is not empty, add it to collapsed
                    
                    if(oneLocusCollapsed[j].getMajor().getBase(k) == '-') { //If collapsed is empty, add it
                        oneLocusCollapsed[j].setMajor(k,oneLocusModels[i][j].getMajor().getBase(k));
                        oneLocusCollapsed[j].setMinor(k,oneLocusModels[i][j].getMinor().getBase(k));
                    } else { //Check that no selection models have different nucleotides at the same positions (this should never happen)
                        
                        if(oneLocusModels[i][j].getMajor().getBase(k) != oneLocusCollapsed[j].getMajor().getBase(k)) {
                            
                            cout << "ERROR in createOneLocusCollapsed! Alleles of one locus models don't match.\n";
                        }
                    }
                    
		    //Locus has been found
                    break; //There should only be one locus per model in oneLocusModels
                }
            }
        }
    }

	return oneLocusCollapsed;   

}

vector<Model> generateModelsFromPreviousBest(vector<DiploidSequence> &collapsedFullHaps, Model &bestModelBefore) {

	vector<Model> result;

	//Loop over each gene and try adding a further selection coefficient or epistasis coefficient to the best model currently (i.e. the one from before)
	for(unsigned int i=0; i<collapsedFullHaps.size(); i++) {

		if(collapsedFullHaps[i].getLength() > 0) { //i.e. there are data for this gene - if not, no point in trying to find new combinations from this gene

			//First, try adding a new selection coefficient
			//Procedure: Find all locus without selection, and add one in turn
			Model::modelSingleGene MSG = bestModelBefore.getModelSingleGene(i);
			DiploidSequence model = MSG.getModel(); //This is the sites currently under selection

			for(int j=0; j<model.getLength(); j++) { //Loop over loci in this gene

				if(model.getMajor().getBase(j) == '-') { //If no selection currently present, add this loci to a model

					DiploidSequence newModel = model;
					newModel.setMajor(j,collapsedFullHaps[i].getMajor().getBase(j));
					newModel.setMinor(j,collapsedFullHaps[i].getMinor().getBase(j));
				
					Model::modelSingleGene newMSG = MSG;
					newMSG.setModel(newModel);
					newMSG.setSelectionPresent(true); //Selection is present at this gene (default==false comes from passing a neutral bestModelBefore)

					Model currentM = bestModelBefore;
					currentM.setModelSingleGene(i,newMSG);
					currentM.countNumParamsToBeFitted();

					result.push_back(currentM);

				}
			}

			//Next, try adding a new epistasis coefficient
			//First find the positions under selection
			vector<int> positionsUnderSel;
			for(int j=0; j<model.getLength(); j++) {

				if(model.getMajor().getBase(j) != '-') { //Selection present
					positionsUnderSel.push_back(j);
				}
			}

			//Next, find all j-way epistasis
			for(unsigned int j=2; j<=positionsUnderSel.size(); j++) { //Requires at least two selection parameters to have epistasis

				//Finds all combos of length j of the elements of positionsUnderSel
				//E.g. if positionsUnderSel has length 5 and j=2, then get (0,1), (0,2), (0,3), (0,4), (1,2), (1,3), (1,4), (2,3), (2,4), (3,4)
				//and if j=3 then get (0,1,2), (0,1,3), (0,1,4), (0,2,3), (0,2,4), (0,3,4), (1,2,3), (1,2,4), (1,3,4), (2,3,4)
				vector<vector<int> > combs = getAllCombs((int) positionsUnderSel.size(), j);

				for(unsigned int k=0; k<combs.size(); k++) { //Loop over combinations

		
					vector<Model::epistasis> epiModelCurrent = MSG.getEpiModel();
					bool combinationFound = false;
					for(unsigned int l=0; l<epiModelCurrent.size(); l++) { //Loop over epistasis coefficients

						if(epiModelCurrent[l].getDim() == (int) j) {


							//Convert a combination into a position e.g. a combination (1,3)
							//refers to a combination of the 1st and 3rd index of positionsUnderSel.
							//So if positionUnderSel = (0,2,3,5,8) then combPos = (2,5)
							vector<int> combPos;
							for(unsigned int m=0; m<combs[k].size(); m++) {
								combPos.push_back(positionsUnderSel[combs[k][m]]);
							}
	
							//Check if current combinations is identical with existing epistasis
							vector<int> epiCurrentPos = epiModelCurrent[l].getPositions();
							if(identicalIntVectors(combPos,epiCurrentPos) == true) { //Combination already included

								combinationFound = true; break;
							}
						}
					}

					if(combinationFound == false) { //Add combination

						Model::epistasis epiCurrent;
						vector<int> epiLociCurrent;
						for(unsigned int l=0; l<combs[k].size(); l++) {
							epiLociCurrent.push_back(positionsUnderSel[combs[k][l]]);
						}
						epiCurrent.setPositions(epiLociCurrent);
						epiCurrent.setDim(j);
						epiModelCurrent.push_back(epiCurrent);
				
						Model::modelSingleGene newMSG = MSG;
						newMSG.setEpiModel(epiModelCurrent);

						Model currentM = bestModelBefore;
						currentM.setModelSingleGene(i,newMSG);
						currentM.countNumParamsToBeFitted();

						result.push_back(currentM);
					}		
				}
			}
		} 
	}


	return result;

}

vector<ModelMR> generateModelsFromPreviousBest(vector<DiploidSequence> &collapsedFullHaps, ModelMR &bestModelBefore) {

	vector<ModelMR> result;
	
	//Loop over each gene and try adding a further selection coefficient or epistasis coefficient to the best model currently (i.e. the one from before)
	for(unsigned int i=0; i<collapsedFullHaps.size(); i++) {

		//First, try adding a new selection coefficient
		//Procedure: Find all locus without selection, and add one in turn
		Model::modelSingleGene MSG = bestModelBefore.getModelSingleGene(i);
		DiploidSequence selModel = MSG.getModel(); //This is the sites currently under selection

		for(int j=0; j<selModel.getLength(); j++) { //Loop over loci in this gene

			if(selModel.getMajor().getBase(j) == '-') { //If no selection currently present, add this loci to a model

				DiploidSequence newModel = selModel;
				newModel.setMajor(j,collapsedFullHaps[i].getMajor().getBase(j));
				newModel.setMinor(j,collapsedFullHaps[i].getMinor().getBase(j));
				
				Model::modelSingleGene newMSG = MSG;
				newMSG.setModel(newModel);
				newMSG.setSelectionPresent(true); //Selection is present at this gene (default==false comes from passing a neutral bestModelBefore)

				ModelMR currentM = bestModelBefore;
				currentM.setModelSingleGene(i,newMSG);
				currentM.countNumParamsToBeFitted();

				result.push_back(currentM);

			}
		}

		//Next, try adding a new epistasis coefficient
		//First find the positions under selection
		vector<int> positionsUnderSel;
		for(int j=0; j<selModel.getLength(); j++) {

			if(selModel.getMajor().getBase(j) != '-') { //Selection present
				positionsUnderSel.push_back(j);
			}
		}

		//Next, find all j-way epistasis
		for(unsigned int j=2; j<=positionsUnderSel.size(); j++) { //Requires at least two selection parameters to have epistasis

			//Finds all combos of length j of the elements of positionsUnderSel
			//E.g. if positionsUnderSel has length 5 and j=2, then get (0,1), (0,2), (0,3), (0,4), (1,2), (1,3), (1,4), (2,3), (2,4), (3,4)
			//and if j=3 then get (0,1,2), (0,1,3), (0,1,4), (0,2,3), (0,2,4), (0,3,4), (1,2,3), (1,2,4), (1,3,4), (2,3,4)
			vector<vector<int> > combs = getAllCombs((int) positionsUnderSel.size(), j);

			for(unsigned int k=0; k<combs.size(); k++) { //Loop over combinations

				vector<Model::epistasis> epiModelCurrent = MSG.getEpiModel();
				bool combinationFound = false;
				for(unsigned int l=0; l<epiModelCurrent.size(); l++) { //Loop over epistasis coefficients
	
					if(epiModelCurrent[l].getDim() == (int) j) {

						vector<int> epiCurrentPos = epiModelCurrent[l].getPositions();
						if(identicalIntVectors(combs[k],epiCurrentPos) == true) { //Combination already included

							combinationFound = true; break;
						}
					}
				}

				if(combinationFound == false) { //Add combination

					Model::epistasis epiCurrent;
					vector<int> epiLociCurrent;
					for(unsigned int l=0; l<combs[k].size(); l++) {
						epiLociCurrent.push_back(positionsUnderSel[combs[k][l]]);
					}
					epiCurrent.setPositions(epiLociCurrent);
					epiCurrent.setDim(j);
					epiModelCurrent.push_back(epiCurrent);
				
					Model::modelSingleGene newMSG = MSG;
					newMSG.setEpiModel(epiModelCurrent);

					ModelMR currentM = bestModelBefore;
					currentM.setModelSingleGene(i,newMSG);
					currentM.countNumParamsToBeFitted();

					result.push_back(currentM);
				}		
			}
		}
	}


	return result;

}



int binomialCoeff(int n, int k) {
    
    int result = 1;
    
    for(int i=1; i<=k; i++) {
        
        result*= ((n+1-i)/(double)i);
    }
    
    return result;
}

int logBinCoeffStirling(int n, int k) {
    
    return n*log(n) - k*log(k) -(n-k)*log(n-k);
}


//Finds the next number from start to max where a set alreadyPicked cannot be chosen from
//The number start is the value of the last entry of alreadyPicked +1
//If alreadyPicked[last entry] == max, then -1 is returned
int getNextLeft(vector<int> alreadyPicked, int max) {

	int start = alreadyPicked[alreadyPicked.size()-1] +1; //Current value of last entry +1 is the starting point
	if(start == max+1) { 
		return -1;
	} else {
		for(int i=start; i<=max; i++) {
			bool toBeIncluded = true; 
			for(unsigned int j=0; j<alreadyPicked.size(); j++) {
				if(i==alreadyPicked[j]) {
					toBeIncluded = false;
				}
			}
			if(toBeIncluded == true) {
				return i;
			}
		}
	}


	return -1; //This should never happen
}

//Find all the sequences in set but NOT in subset
vector<Sequence> findRemainingSeqs(vector<Sequence> subset, vector<Sequence> set) {

	vector<Sequence> result;
	for(unsigned int i=0; i<set.size(); i++) {

		bool found = false;
		for(unsigned int j=0; j<subset.size(); j++) {
			if(identicalSeqs(subset[j],set[i]) == true) {
				found = true;
				break;
			}
		}

		if(found == false) {
			result.push_back(set[i]);
		}
	}
	return result;
}


//Takes a vector of full haplotypes and constructs a single diploidSequence where the major and minor alleles correspond
//to the allels with the highest frequencies
//E.g. if the full haplotypes are ACT, AGT, CCA, AGA with freqs 0.10, 0.20, 0.30 0.40
//then the diploidSequence is major=AGA  minor=CCT
DiploidSequence constructDiploidFromFullHap(vector<Sequence> &fhs, vector<double> &freqs) {

	DiploidSequence result = DiploidSequence(fhs[0].getLength()); //Initialise empty diploid sequence
	for(int i=0; i<result.getLength(); i++) { //Loop over bases
	
		char allele1 = fhs[0].getBase(i); //WLOG set the first allele to be the base at position i from the first haplotype
		char allele2 = '-'; //Currently unknown
		double freqAllele1 = 0;
		double freqAllele2 = 0;
		for(unsigned int j=0; j<freqs.size(); j++) { //Loop over the full haplotype frequencies

			if(fhs[j].getBase(i) != allele1) {
				allele2 = fhs[j].getBase(i);
				freqAllele2 += freqs[j];
			} else {
				freqAllele1 += freqs[j];
			}		
		}

		//Check which is major and which is minor
		if(freqAllele1 >= freqAllele2) {
			result.setMajor(i,allele1);
			result.setMinor(i,allele2);
		} else {	
			result.setMajor(i,allele2);
			result.setMinor(i,allele1);
		}
	}

	return result;
}

//Takes a vector of full haplotypes and constructs a single diploidSequence with major = fhs[0]
DiploidSequence constructDiploidFromFullHap(vector<Sequence> &fhs) {

	if(fhs.size() > 0) { //If there are any full haplotypes

		DiploidSequence result = DiploidSequence(fhs[0].getLength()); //Initialise empty diploid sequence
		for(int i=0; i<result.getLength(); i++) { //Loop over bases
	
			char allele1 = fhs[0].getBase(i);
			char allele2 = '-'; //Currently unknown
			for(unsigned int j=0; j<fhs.size(); j++) { //Loop over the full haplotypes

				if(fhs[j].getBase(i) != allele1) {
					allele2 = fhs[j].getBase(i);
					break;
				}		
			}
		
			result.setMajor(i,allele1);
			result.setMinor(i,allele2);
		}
		
		return result;
	
	} else { //This gene is empty, so return empty diploid sequence

		DiploidSequence empty;
		return empty;
	}
}





//Check that none of the entres of the vector v are the same
////Exception: Zero allowed multiple times.
bool noEntryTheSameExceptZero(vector<double> &v) {


	for(unsigned int i=0; i<v.size()-1; i++) {

		double currentVal = v[i];
		if(currentVal != 0) {
			for(unsigned int j=i+1; j<v.size(); j++) {
		
				if(currentVal == v[j]) { return	false; }
			}	
		}
	}

	return true;
}



//Combinatorics code based on code from Rosetta Code http://rosettacode.org/wiki/Combinations#C.2B.2B
////If N=5 and k=2, then get (0,1), (0,2), (0,3), (0,4), (1,2), (1,3), (1,4), (2,3), (2,4), (3,4)
////and if k=3 then get (0,1,2), (0,1,3), (0,1,4), (0,2,3), (0,2,4), (0,3,4), (1,2,3), (1,2,4), (1,3,4), (2,3,4)
vector<vector<int> > getAllCombs(int N, int k) {

	vector<vector<int> > result;

	string bitmask(k, 1); // K leading 1's
	bitmask.resize(N, 0); // N-K trailing 0's
	//Store integers and permute bitmask
	do {
		vector<int> currentComb;
        	for (int i = 0; i < N; ++i) { // [0..N-1] integers
			if (bitmask[i]) {
				currentComb.push_back(i);
			}
		}
		result.push_back(currentComb);
	} while (prev_permutation(bitmask.begin(), bitmask.end()));

	return result;
}



vector<vector<int> > getLinkedPairs(vector<Sequence> &haps) {

	vector<vector<int> > result;

	int numLoci = haps[0].getLength();
	for(int i=0; i<numLoci-1; i++) {
		for(int j=i+1; j<numLoci; j++) {

			bool linked = true;
			char i0 = haps[0].getBase(i);
			char j0 = haps[0].getBase(j);
			
			//Go through all haps and see if linked
			for(unsigned int k=1; k<haps.size(); k++) {

				if(haps[k].getBase(i) == i0 && haps[k].getBase(j) != j0) {
					linked = false;
					break;

				} else if(haps[k].getBase(i) != i0 && haps[k].getBase(j) == j0) {
					linked = false;
					break;
				}
			}


			if(linked == true) { //Position i and j is a linked pair

				vector<int> pair = {i,j};
				result.push_back(pair);
			}
		}
	}


	return result;
}

vector<string> split(const string& s, char delim) {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while(getline(ss,item,delim)) {
		tokens.push_back(item);
	}
	return tokens;
}


//Related to gaining information on memory usage
int parseLine(char* line){
	// This assumes that a digit will be found and the line ends in " Kb".

	 int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getValue(){ //Note: this value is in KB!

FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}


bool stringToBool(string& s) {


	if(s.compare("true") == 0 || s.compare("True") == 0 || s.compare("TRUE") == 0 || s.compare("1") == 0) { return true; }
	else if(s.compare("false") == 0 || s.compare("False") == 0 || s.compare("FALSE") == 0 || s.compare("0") == 0) { return false; }
	else{
		cout << "ERROR! argument: " << s << " in Misc:stringToBool(string& s) is not of the right form. Returning false by default.";
		return false;
	}
}

//Compares two vectors by size and returns true if b>a
bool compareBySize(const vector<int>& a, const vector<int>& b) {

	return (a.size() > b.size());

}


//Gamma and delta functions for compound distributions

//Computes 
//\begin{equation}
//\gamma_n=\frac{(-1)^n \lambda ^{-\frac{n^2}{2}-\frac{n}{2}} \left(N^T\right)^{-n-1} (\lambda 
//   N^T;\lambda )_n \left(\lambda  \left(N^T\right)^2 \left(\sum\limits_{j=0}^{n-1}
//      \frac{(\lambda  N^T-1) (-1)^{1-j} \lambda^{\frac{j^2}{2}+\frac{j}{2}-1} \left(N^T\right)^{j-1}}{(\lambda  N^T;\lambda)_{j+1}}\right)+\lambda  N^T-1\right)}{\lambda  N^T-1}
//      \end{equation}
//
// where lambda = growth factor
double gamma_n(int n, double lambda, int Nt) {

	double gamma = pow(-1,n)*pow(lambda,-(n*n)/2.0-n/2.0)*pow(Nt,-n-1)*qPochhammer(lambda*Nt,lambda,n)/(lambda*Nt-1); //Consider if division by integers is a problem for large lambda,Nt

	double sum = 0;
	for(int j=0; j<=n-1; j++) {

		sum += (lambda*Nt-1)*pow(-1,1-j)*pow(lambda,j*j/2.0+j/2.0-1)*pow(Nt,j-1)/qPochhammer(lambda*Nt,lambda,j+1);
	}

	gamma *= (lambda*Nt*Nt*sum + lambda*Nt -1);


	return gamma;
}

//Computes 
//\begin{equation}
//\delta_n=(-1)^n \lambda ^{-\frac{n^2}{2}-\frac{3 n}{2}} \left(N^T\right)^{-n-1} \left(N^T \lambda^n-1\right) (\lambda  N^T;\lambda )_n
//\end{equation}
//
// where lambda = growth factor
double delta_n(int n, double lambda, int Nt) {

	double delta = pow(-1,n)*pow(lambda,-(n*n)/2.0-n/2.0)*pow(Nt,-n-1)*(Nt-1)*qPochhammer(lambda*Nt,lambda,n);

	return delta;
}

//q-Pochhammer symbol: (a,q)_n = prod_0^{n-1} (1-a*q^k), with (a,q)_0 = 1
double qPochhammer(double a, double q, int n) {

	if(n == 0) {

		return 1;

	} else if(n > 0) {

		double prod = 1;
		for(int k=0; k<=n-1; k++) {

			prod *= (1-a*pow(q,k));
		}
		return prod;
	} else {

		cout << "Error in qPochhammer function: n is < 0: " << n << "\n";
		exit(1);
	}

}











    

void constructFullHaps(vector<Sequence> *phs, vector<Sequence> *fhs) {
    
    //Changing sequences in vectors of chars to work with Chris' code
 //   cout << "Partial haps:\n";
    vector<vector<char> > haps;
    for(unsigned int i=0; i<phs->size(); i++) {
        
    //    (*phs)[i].print();
        
        vector<char> h;
        for(int j=0; j< (*phs)[i].getLength(); j++) {
            h.push_back((*phs)[i].getBase(j));
        }
        
        haps.push_back(h);
    }

    
    //Cycle through overlap steps
    vector<vector<char> > haps2=haps;
//    for(unsigned int i=0; i<haps.size();i++) {
//        cout << "Char converted partial hap is: ";
//        for(unsigned int j=0; j<haps[i].size();j++) {
//            cout << haps[i][j];
//        }
//        cout << "\n";
//    }
    
    //Matching overlap joins
    for (int i=0;i<200;i++) { //Change to while loop and include a stopping criteria?
        haps2=haps;
        OverlapStepShared1(haps); //Remove haplotypes contained within each other
        OverlapStepShared2(haps);
        if (haps==haps2) {
            break;
        }
    }
    
    //Non-matching overlap joins
    for (int i=0;i<20;i++) {
        haps2=haps;
        OverlapStepNoShare(haps);
        OverlapStepShared1(haps);
        OverlapStepShared2(haps);
        if (haps==haps2) {
            break;
        }
    }
    
    
    
    //Non-overlap joins
    vector<par> sf;
    GetStartFinish(sf,haps);
    vector< vector<char> > new_haps;
    BuildPaths(sf,haps,new_haps);
    haps=new_haps;
    DuplicateStep(haps);
    
    
    //Converting from vector<char> to Sequence
  //  cout << "Full haps:\n";
    for(unsigned int i=0; i<haps.size(); i++) {
        Sequence s;
        for(unsigned int j=0; j<haps[i].size(); j++) {
            s.addBase(haps[i][j]);
        }
        //s.print();
        fhs->push_back(s);
    }
    
    
}

//Method for updating haps following a step (e.g. overlap step, combination step, etc.)
void ResetHaps (vector<int> incl, vector< vector<char> >& haps) {
    vector< vector<char> > new_haps;
    for (unsigned int i=0;i<haps.size();i++) {
        if (incl[i]==1) {
            new_haps.push_back(haps[i]);
        }
    }
    haps=new_haps;
}


//Remove duplicate haplotypes
void DuplicateStep (vector< vector<char> >& haps) {
    vector< vector<char> > new_haps;
    vector<int> uniq;
    for (unsigned int i=0;i<haps.size();i++) {
        uniq.push_back(1);
    }
    //cout << "Haplotypes " << haps.size() << "\n";
    for (unsigned int i=0;i<haps.size();i++) { //Simply loop over all combinations i,j>i and check if any of them are duplicates
        for (unsigned int j=i;j<haps.size();j++) {
            if (i!=j&&haps[i]==haps[j]) {
                uniq[j]=0;
            }
        }
    }
    for (unsigned int i=0;i<haps.size();i++) {
        if (uniq[i]==1) {
            new_haps.push_back(haps[i]);
        }
    }
    // cout << "Unique haplotypes " << new_haps.size() << "\n";
    haps=new_haps;
}

//Remove haplotypes contained within each other
void OverlapStepShared1 (vector< vector<char> >& haps) {
    //One contained within another - try doing these first before other overlaps...
    vector<int> incl (haps.size(),1); //Haplotypes that should be kept have a 1, those to be removed have a 0
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=0;j<size;j++) {
            if (i!=j) {
                int match=1;   //Match at shared positions
                int n_match=0;  //Number of shared positions
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    //Check whether j is contained in i.
                    if (haps[j][k]!='-') { //Here we are only considering the alleles k in hap j that isn't '-'
                        if (haps[i][k]!='-') {
                            n_match++;
                        }
                        if (haps[j][k]!=haps[i][k]) { //If just a single one of the non-'-' alleles in hap j is not identical to hap i, then no match
                            match=0;
                        }
                    }
                }
                if (match==1&&n_match>0) {
                    //Remove haps[j]
                    incl[j]=0;
                }
            }
        }
    }
    ResetHaps(incl,haps); //Reset haps, i.e. only keep haps with a 1 in incl
    DuplicateStep(haps); //Remote any possible duplicate haplotypes
}



//Remove shared overlap like AACC-- --CCTT
void OverlapStepShared2 (vector< vector<char> >& haps) {
    vector<int> incl (haps.size(),1);
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=i+1;j<size;j++) {
            int match=1;   //Match at shared positions
            int n_match=0;  //Number of shared positions
            for	(unsigned int k=0;k<haps[i].size();k++) {
                //Check whether i and j have the same nucleotides wherever they overlap.
                if (haps[j][k]!='-'&&haps[i][k]!='-') { //Number of overlaps
                    n_match++;
                    if (haps[j][k]!=haps[i][k]) {
                        match=0;
                    }
                }
            }
            if (match==1&&n_match>0) {
//                for	(int k=0;k<haps[i].size();k++) {
//                    cout << haps[i][k];
//                }
//                cout << " overlaps ";
//                for	(int k=0;k<haps[j].size();k++) {
//                    cout << haps[j][k];
//                }
//                cout << "\n";
                vector<char> newhap;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[j][k]!='-') {
                        newhap.push_back(haps[j][k]);
                    } else if (haps[i][k]!='-') {
                        newhap.push_back(haps[i][k]);
                    } else {
                        newhap.push_back('-');
                    }
                }
                //Add newhap, remove the other two
                haps.push_back(newhap);
                incl.push_back(1);
                incl[i]=0;
                incl[j]=0;
            }
        }
    }
    
    //Update haps and remove any possible duplicates
    ResetHaps(incl,haps);
    DuplicateStep(haps);
}




//Non-matching overlaps
void OverlapStepNoShare (vector< vector<char> >& haps) {
    
    vector<int> incl (haps.size(),1);
    int size=haps.size();
    for (int i=0;i<size;i++) {
        for (int j=i+1;j<size;j++) {
            int match=1;   //Match at shared positions
            int n_match=0;  //Number of shared positions
            for	(unsigned int k=0;k<haps[i].size();k++) {
                //Check whether i and j have the same nucleotides wherever they overlap.  N.B. Must have at least one locus not in common
                if (haps[j][k]!='-'&&haps[i][k]!='-') { //Number of overlaps
                    n_match++;
                    if (haps[j][k]!=haps[i][k]) {
                        match=0;
                    }
                }
            }
            int n_over=0;
            if (match==0&&n_match<(int) haps[i].size()&&n_match>0) { //some shared positions, but not a complete match
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[j][k]!='-'&&haps[i][k]=='-') {
                        n_over=1; //There is an overlap of some kind
                    }
                    if (haps[i][k]!='-'&&haps[j][k]=='-') {
                        n_over=1; //There is an overlap of some kind
                    }
                }
            }
            
            //Overlap present
            if (n_over==1) {
                
                vector<char> nh_start;
                //Make beginning
                int ov=0;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[i][k]!='-'&&haps[j][k]!='-') {
                        ov=1;
                    }
                    if (ov==0) {
                        if (haps[j][k]!='-') {
                            nh_start.push_back(haps[j][k]);
                        } else if (haps[i][k]!='-') {
                            nh_start.push_back(haps[i][k]);
                        } else {
                            nh_start.push_back('-');
                        }
                    } else {
                        nh_start.push_back('-');
                    }
                }
                
                //Make end
                vector<char> nh_end;
                ov=0;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (haps[i][k]!='-'&&haps[j][k]!='-') {
                        ov=1;
                    }
                    if (ov==1) {
                        if (haps[j][k]!='-'&&haps[i][k]=='-') {
                            nh_end.push_back(haps[j][k]);
                        } else if (haps[i][k]!='-'&&haps[j][k]=='-') {
                            nh_end.push_back(haps[i][k]);
                        } else {
                            nh_end.push_back('-');
                        }
                    } else {
                        nh_end.push_back('-');
                    }
                }
                

		     //Make c1 and c2
                vector<char> nh_c1;
                vector<char> nh_c2;
                for	(unsigned int k=0;k<haps[i].size();k++) {
                    if (nh_start[k]=='-'&&nh_end[k]=='-') {
                        nh_c1.push_back(haps[i][k]);
                        nh_c2.push_back(haps[j][k]);
                    } else {
                        nh_c1.push_back('-');
                        nh_c2.push_back('-');
                    }
                }
                
                //Construct start + c1 + end
                vector<char> newhap1;
                vector<char> newhap2;
                for (unsigned int k=0;k<haps[i].size();k++){
                    if (nh_start[k]!='-') {
                        newhap1.push_back(nh_start[k]);
                        newhap2.push_back(nh_start[k]);
                    } else if (nh_c1[k]!='-') {
                        newhap1.push_back(nh_c1[k]);
                        newhap2.push_back(nh_c2[k]);
                    } else if (nh_end[k]!='-') {
                        newhap1.push_back(nh_end[k]);
                        newhap2.push_back(nh_end[k]);
                    } else {
                        newhap1.push_back('-');
                        newhap2.push_back('-');
                    }
                }
                
                haps.push_back(newhap1);
                haps.push_back(newhap2);
                incl.push_back(1);
                incl.push_back(1);
                //cout << newhap1.size() << " " << newhap2.size() << "\n";
                incl[i]=0;
                incl[j]=0;
            }
        }
    }
    ResetHaps(incl,haps);
    DuplicateStep(haps);
}



void GetStartFinish(vector<par>& sf, vector< vector<char> >& haps) {
    for (unsigned int i=0;i<haps.size();i++) {
        par p;
        p.i1=-1;
        p.i2=-1;
        for (unsigned int j=0;j<haps[i].size();j++) {
            //cout << "i " << i << " j " << j << " " << haps[i][j] << "\n";
            if (haps[i][j]!='-'&&p.i1<0) {
                p.i1=j;
                //	cout << "Found start\n";
            }
            if (haps[i][j]=='-'&&p.i1>=0&&p.i2<0) {
                p.i2=j-1;
                //	cout << "Found end\n";
            }
        }
        if (p.i2<0) {
            p.i2=haps[i].size();
        }
        sf.push_back(p);
    }
}




void AddPath (int prev, int start, vector<char> v, vector<par>& sf, vector< vector<char> >& haps, vector< vector<char> >& new_haps) {
    //Add last haplotype to vector
    vector<char> v2=v;
    //cout << "Previous is " << prev << " " << sf[prev].i1 << " " << sf[prev].i2 << "\n";
    for (int j=sf[prev].i1;j<=sf[prev].i2;j++) {
        v2.push_back(haps[prev][j]);
    }
    //cout << "Vector v is: ";
    //for (int i=0;i<v2.size();i++) {
    //	cout << v2[i];
    //}
    //cout << "\n";
    
    //Check if this is the end of the haplotype and if so push to new_haps
    if (sf[prev].i2>=(int) haps[prev].size()) {
        //	cout << "Doing push_back\n";
        vector<char> v3;
        for (unsigned int i=0;i<haps[prev].size();i++) {//Trim length
            v3.push_back(v2[i]);
        }
        new_haps.push_back(v3);
    } else {
        //If not search for new haplotype and recall
        for (unsigned int i=0;i<sf.size();i++) {
            if (sf[i].i1==start) {
                AddPath(i,sf[i].i2+1,v2,sf,haps,new_haps);
            }
        }
    }
}

void BuildPaths(vector<par>& sf, vector< vector<char> > haps, vector< vector<char> >& new_haps) {
    vector<char> v;
    for (unsigned int i=0;i<sf.size();i++) {
        if (sf[i].i1==0) {
            //cout << "Detected " << i << "\n";
            AddPath(i,sf[i].i2+1,v,sf,haps,new_haps);
        }
    }
}


